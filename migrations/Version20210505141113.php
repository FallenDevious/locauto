<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505141113 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vehicle ADD type_carbu_id INT NOT NULL, ADD agency_id INT NOT NULL');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486BB81DCF7 FOREIGN KEY (type_carbu_id) REFERENCES type_carbu (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486CDEADB2A FOREIGN KEY (agency_id) REFERENCES agency (id)');
        $this->addSql('CREATE INDEX IDX_1B80E486BB81DCF7 ON vehicle (type_carbu_id)');
        $this->addSql('CREATE INDEX IDX_1B80E486CDEADB2A ON vehicle (agency_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486BB81DCF7');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486CDEADB2A');
        $this->addSql('DROP INDEX IDX_1B80E486BB81DCF7 ON vehicle');
        $this->addSql('DROP INDEX IDX_1B80E486CDEADB2A ON vehicle');
        $this->addSql('ALTER TABLE vehicle DROP type_carbu_id, DROP agency_id');
    }
}
