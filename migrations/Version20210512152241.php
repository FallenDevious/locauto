<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210512152241 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, taken_insurance TINYINT(1) NOT NULL, estimated_price DOUBLE PRECISION NOT NULL, return_date DATETIME DEFAULT NULL, km_return DOUBLE PRECISION DEFAULT NULL, price_real DOUBLE PRECISION DEFAULT NULL, name_creat VARCHAR(255) NOT NULL, date_creat DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reserve CHANGE vehicle_id vehicle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle ADD reservation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id)');
        $this->addSql('CREATE INDEX IDX_1B80E486B83297E7 ON vehicle (reservation_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486B83297E7');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('ALTER TABLE reserve CHANGE vehicle_id vehicle_id INT NOT NULL');
        $this->addSql('DROP INDEX IDX_1B80E486B83297E7 ON vehicle');
        $this->addSql('ALTER TABLE vehicle DROP reservation_id');
    }
}
