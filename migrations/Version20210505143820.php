<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505143820 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reserve (id INT AUTO_INCREMENT NOT NULL, vehicle_id INT DEFAULT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, taken_insurance TINYINT(1) NOT NULL, estimated_price DOUBLE PRECISION NOT NULL, return_date DATETIME DEFAULT NULL, km_return DOUBLE PRECISION DEFAULT NULL, price_real DOUBLE PRECISION DEFAULT NULL, name_creat VARCHAR(100) NOT NULL, date_creat DATETIME NOT NULL, INDEX IDX_1FE0EA22545317D1 (vehicle_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reserve ADD CONSTRAINT FK_1FE0EA22545317D1 FOREIGN KEY (vehicle_id) REFERENCES vehicle (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE reserve');
    }
}
