<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/reservationAdmin.html.twig */
class __TwigTemplate_8ae840f37b905892523af5c55ce21da64b9f6bfe9050e58b91c3d32abc09a1c6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/reservationAdmin.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/reservationAdmin.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/reservationAdmin.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Acceuil";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"container-fluid red\">
   ";
        // line 7
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "user", [], "any", false, false, false, 7)) {
            // line 8
            echo "        <h1>Réservez votre véhicule</h1>
        <div class=\"container\">
        ";
            // line 10
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 11
                echo "            
        ";
            }
            // line 13
            echo "          ";
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 13, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("confirm_reserve")]);
            echo "
          <div class=\"row align-items-center\">
            <div class=\"row col FormReserve\">
               <div class=\" col\">
                ";
            // line 17
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 17, $this->source); })()), "categorie", [], "any", false, false, false, 17), 'row', ["label" => "choix categorie"]);
            echo "
              </div>
              <div class=\" col \">
                  ";
            // line 20
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 20, $this->source); })()), "vehicle", [], "any", false, false, false, 20), 'row', ["label" => "choix vehicule*"]);
            echo "  
              </div>
              <div class=\" col\">
                ";
            // line 23
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 23, $this->source); })()), "startDate", [], "any", false, false, false, 23), 'row', ["label" => "Date de départ"]);
            echo "
              </div>
              <div class=\"col\">
                ";
            // line 26
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 26, $this->source); })()), "endDate", [], "any", false, false, false, 26), 'row', ["label" => "Date Retour"]);
            echo "
              </div>
              <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
                  ";
            // line 29
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 29, $this->source); })()), "takenInsurance", [], "any", false, false, false, 29), 'row', ["label" => "Voulez vous prendre l'assurance"]);
            echo "
              </div>
              <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
                <button type=\"submit\" class=\"btn btn-success\"> Reserver !</button>
              </div>
                <p>Un choix de véhicule selon la catégorie choisie vous sera proposé en agence</p>
                <p>le tarif week-end est automatiquement prise en compte pour une periode comprise du vendredi 16h au Dimanche 19h</p>
            </div>
            <div class=\"col\">
                <img src=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/eco.png"), "html", null, true);
            echo "\" width=\"400\" height=\"300\" >
            </div>
          </div>
            ";
            // line 41
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 41, $this->source); })()), 'form_end');
            echo "
        </div>
        ";
        } else {
            // line 44
            echo "        <div class=\"container-fluid red\">
          <h1>Vous devez vous connecter pour créer une reservation</h1>
        </div>
        ";
        }
        // line 48
        echo "
    </div>
    <div class=\"alert alert-primary\" role=\"alert\">
      promotion : 10% de réduction pour 3 jour de location.
    </div>
  <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
  ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 54, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 55
            echo "        <div class=\"col\">
          <div class=\"card mb-4 rounded-3 shadow-sm\">
            <div class=\"card-header py-3\">
              <h4 class=\"my-0 fw-normal\">";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "label", [], "any", false, false, false, 58), "html", null, true);
            echo "</h4>
            </div>
            <div class=\"card-body\">
            <img src=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/eco.png"), "html", null, true);
            echo "\" width=\"300\" height=\"200\" >
              <h1 class=\"card-title pricing-card-title\">";
            // line 62
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "priceday", [], "any", false, false, false, 62), "html", null, true);
            echo "€<small class=\"text-muted fw-light\">/jour</small></h1>
              <ul class=\"list-unstyled mt-3 mb-4\">
                <li>";
            // line 64
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "priceWeekEnd", [], "any", false, false, false, 64), "html", null, true);
            echo "€ en Week-end</li>
                <li>";
            // line 65
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "priceInsurance", [], "any", false, false, false, 65), "html", null, true);
            echo "€ d'assurance</li>
                <li>";
            // line 66
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "guarantee", [], "any", false, false, false, 66), "html", null, true);
            echo "€ de caution</li>
              </ul>
            </div>
          </div>
        </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "  </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/reservationAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 72,  205 => 66,  201 => 65,  197 => 64,  192 => 62,  188 => 61,  182 => 58,  177 => 55,  173 => 54,  165 => 48,  159 => 44,  153 => 41,  147 => 38,  135 => 29,  129 => 26,  123 => 23,  117 => 20,  111 => 17,  103 => 13,  99 => 11,  97 => 10,  93 => 8,  91 => 7,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Acceuil{% endblock %}

{% block body %}
    <div class=\"container-fluid red\">
   {% if app.user %}
        <h1>Réservez votre véhicule</h1>
        <div class=\"container\">
        {% if is_granted('ROLE_ADMIN') %}
            
        {% endif %}
          {{ form_start( formReservation,{'action': path('confirm_reserve')},{'attr': {'id': 'confirm_form'}} ) }}
          <div class=\"row align-items-center\">
            <div class=\"row col FormReserve\">
               <div class=\" col\">
                {{ form_row(formReservation.categorie,{'label':'choix categorie'}) }}
              </div>
              <div class=\" col \">
                  {{ form_row(formReservation.vehicle,{'label':'choix vehicule*'}) }}  
              </div>
              <div class=\" col\">
                {{ form_row(formReservation.startDate,{'label':'Date de départ'}) }}
              </div>
              <div class=\"col\">
                {{ form_row(formReservation.endDate,{'label':'Date Retour'}) }}
              </div>
              <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
                  {{ form_row(formReservation.takenInsurance,{'label':'Voulez vous prendre l\\'assurance'}) }}
              </div>
              <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
                <button type=\"submit\" class=\"btn btn-success\"> Reserver !</button>
              </div>
                <p>Un choix de véhicule selon la catégorie choisie vous sera proposé en agence</p>
                <p>le tarif week-end est automatiquement prise en compte pour une periode comprise du vendredi 16h au Dimanche 19h</p>
            </div>
            <div class=\"col\">
                <img src=\"{{ asset('build/eco.png') }}\" width=\"400\" height=\"300\" >
            </div>
          </div>
            {{ form_end(formReservation) }}
        </div>
        {% else %}
        <div class=\"container-fluid red\">
          <h1>Vous devez vous connecter pour créer une reservation</h1>
        </div>
        {% endif %}

    </div>
    <div class=\"alert alert-primary\" role=\"alert\">
      promotion : 10% de réduction pour 3 jour de location.
    </div>
  <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
  {% for category in categories %}
        <div class=\"col\">
          <div class=\"card mb-4 rounded-3 shadow-sm\">
            <div class=\"card-header py-3\">
              <h4 class=\"my-0 fw-normal\">{{category.label}}</h4>
            </div>
            <div class=\"card-body\">
            <img src=\"{{ asset('build/eco.png') }}\" width=\"300\" height=\"200\" >
              <h1 class=\"card-title pricing-card-title\">{{category.priceday}}€<small class=\"text-muted fw-light\">/jour</small></h1>
              <ul class=\"list-unstyled mt-3 mb-4\">
                <li>{{category.priceWeekEnd}}€ en Week-end</li>
                <li>{{category.priceInsurance}}€ d'assurance</li>
                <li>{{category.guarantee}}€ de caution</li>
              </ul>
            </div>
          </div>
        </div>
  {% endfor %}
  </div>
</div>
{% endblock %}
", "admin/reservationAdmin.html.twig", "C:\\symfony\\locauto\\templates\\admin\\reservationAdmin.html.twig");
    }
}
