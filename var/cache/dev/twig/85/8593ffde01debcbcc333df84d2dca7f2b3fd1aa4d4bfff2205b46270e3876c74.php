<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/reservationProgress.html.twig */
class __TwigTemplate_22f4eaab0c1518a1ed6640595b623c5caa4bddbebbc0930e302d26a238e71723 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/reservationProgress.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/reservationProgress.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/reservationProgress.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "reservations en cours";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1>Liste des reservation en cours</h1>
    <table class=\"table\">
        <thead>
            <th scope=\"col\">numero de reservation</th>
            <th scope=\"col\">date de debut</th>
            <th scope=\"col\">date de fin</th>
            <th scope=\"col\">assurance prise</th>
            <th scope=\"col\">prix estimé</th>
            <th scope=\"col\">date retour du véhicule</th>
            <th scope=\"col\">km retour</th>
            <th scope=\"col\">prix total</th>
            <th scope=\"col\">vehicule louer</th>
            <th scope=\"col\">identifiant client</th>
            <th scope=\"col\">Nom du client</th>
        </thead>
        <tbody>
            ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reservations"]) || array_key_exists("reservations", $context) ? $context["reservations"] : (function () { throw new RuntimeError('Variable "reservations" does not exist.', 22, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["reservation"]) {
            // line 23
            echo "        ";
            if ( !twig_get_attribute($this->env, $this->source, $context["reservation"], "returnDate", [], "any", false, false, false, 23)) {
                // line 24
                echo "                <tr>
                    <td>";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "id", [], "any", false, false, false, 25), "html", null, true);
                echo "</td>
                    <td>";
                // line 26
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "startDate", [], "any", false, false, false, 26), "d/m/Y"), "html", null, true);
                echo "</td>
                    <td>";
                // line 27
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "endDate", [], "any", false, false, false, 27), "d/m/Y"), "html", null, true);
                echo "</td>
                    
                    <td>
                        ";
                // line 30
                if ((twig_get_attribute($this->env, $this->source, $context["reservation"], "takenInsurance", [], "any", false, false, false, 30) == 1)) {
                    // line 31
                    echo "                            OUI
                        ";
                } else {
                    // line 33
                    echo "                        NON
                        ";
                }
                // line 34
                echo " 
                    </td>
                    <td>";
                // line 36
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "estimatedPrice", [], "any", false, false, false, 36), "html", null, true);
                echo "</td>
                    ";
                // line 37
                if (twig_get_attribute($this->env, $this->source, $context["reservation"], "returnDate", [], "any", false, false, false, 37)) {
                    // line 38
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "returnDate", [], "any", false, false, false, 38), "d/m/Y"), "html", null, true);
                    echo "</td>
                    ";
                } else {
                    // line 40
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "returnDate", [], "any", false, false, false, 40), "html", null, true);
                    echo "</td>
                    ";
                }
                // line 42
                echo "                    <td>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "kmReturn", [], "any", false, false, false, 42), "html", null, true);
                echo "</td>
                    <td>";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "priceReal", [], "any", false, false, false, 43), "html", null, true);
                echo "</td>
                    ";
                // line 44
                if (twig_get_attribute($this->env, $this->source, $context["reservation"], "vehicle", [], "any", false, false, false, 44)) {
                    // line 45
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reservation"], "vehicle", [], "any", false, false, false, 45), "brand", [], "any", false, false, false, 45), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reservation"], "vehicle", [], "any", false, false, false, 45), "model", [], "any", false, false, false, 45), "html", null, true);
                    echo "</td>
                    ";
                } else {
                    // line 47
                    echo "                    <td>en cour de validation</td>
                    ";
                }
                // line 49
                echo "                    <td>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reservation"], "user", [], "any", false, false, false, 49), "id", [], "any", false, false, false, 49), "html", null, true);
                echo "</td>
                    <td>";
                // line 50
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reservation"], "user", [], "any", false, false, false, 50), "username", [], "any", false, false, false, 50), "html", null, true);
                echo "</td>
                      ";
                // line 51
                if ( !twig_get_attribute($this->env, $this->source, $context["reservation"], "kmReturn", [], "any", false, false, false, 51)) {
                    // line 52
                    echo "                        ";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reservation"], "vehicle", [], "any", false, false, false, 52), "rent", [], "any", false, false, false, 52) == false)) {
                        // line 53
                        echo "                            <td>
                            <a href=\"";
                        // line 54
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_confirmLoc", ["id" => twig_get_attribute($this->env, $this->source, $context["reservation"], "id", [], "any", false, false, false, 54)]), "html", null, true);
                        echo "\">confirmer location</a>
                            </td>
                            <td>
                            confirmer la location !
                            </td>
                            ";
                    } else {
                        // line 60
                        echo "                            <td>
                                confirmé !
                            <td>
                                <a href=\"";
                        // line 63
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_userReversationEdit", ["id" => twig_get_attribute($this->env, $this->source, $context["reservation"], "id", [], "any", false, false, false, 63)]), "html", null, true);
                        echo "\">conclure location</a>
                            </td>
                            </td>
                        ";
                    }
                    // line 67
                    echo "                    ";
                }
                // line 68
                echo "                </tr>
        ";
            }
            // line 70
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reservation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "            
        </tbody>
                    <td><a href=\"";
        // line 73
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_userList");
        echo "\" class=\"btn btn-warning\">Retour</a></td>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/reservationProgress.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 73,  236 => 71,  230 => 70,  226 => 68,  223 => 67,  216 => 63,  211 => 60,  202 => 54,  199 => 53,  196 => 52,  194 => 51,  190 => 50,  185 => 49,  181 => 47,  173 => 45,  171 => 44,  167 => 43,  162 => 42,  156 => 40,  150 => 38,  148 => 37,  144 => 36,  140 => 34,  136 => 33,  132 => 31,  130 => 30,  124 => 27,  120 => 26,  116 => 25,  113 => 24,  110 => 23,  106 => 22,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block title %}reservations en cours{% endblock %}

{% block body %}
    <h1>Liste des reservation en cours</h1>
    <table class=\"table\">
        <thead>
            <th scope=\"col\">numero de reservation</th>
            <th scope=\"col\">date de debut</th>
            <th scope=\"col\">date de fin</th>
            <th scope=\"col\">assurance prise</th>
            <th scope=\"col\">prix estimé</th>
            <th scope=\"col\">date retour du véhicule</th>
            <th scope=\"col\">km retour</th>
            <th scope=\"col\">prix total</th>
            <th scope=\"col\">vehicule louer</th>
            <th scope=\"col\">identifiant client</th>
            <th scope=\"col\">Nom du client</th>
        </thead>
        <tbody>
            {% for reservation in reservations %}
        {% if not reservation.returnDate %}
                <tr>
                    <td>{{reservation.id}}</td>
                    <td>{{reservation.startDate | date('d/m/Y')}}</td>
                    <td>{{reservation.endDate | date('d/m/Y')}}</td>
                    
                    <td>
                        {% if reservation.takenInsurance ==  1 %}
                            OUI
                        {% else %}
                        NON
                        {% endif %} 
                    </td>
                    <td>{{reservation.estimatedPrice}}</td>
                    {% if reservation.returnDate %}
                    <td>{{reservation.returnDate | date('d/m/Y')}}</td>
                    {% else %}
                    <td>{{reservation.returnDate}}</td>
                    {% endif %}
                    <td>{{reservation.kmReturn}}</td>
                    <td>{{reservation.priceReal}}</td>
                    {% if reservation.vehicle %}
                    <td>{{reservation.vehicle.brand}} {{reservation.vehicle.model}}</td>
                    {% else %}
                    <td>en cour de validation</td>
                    {% endif %}
                    <td>{{reservation.user.id}}</td>
                    <td>{{reservation.user.username}}</td>
                      {% if not reservation.kmReturn %}
                        {% if reservation.vehicle.rent == false %}
                            <td>
                            <a href=\"{{path('admin_confirmLoc',{'id': reservation.id})}}\">confirmer location</a>
                            </td>
                            <td>
                            confirmer la location !
                            </td>
                            {% else %}
                            <td>
                                confirmé !
                            <td>
                                <a href=\"{{path('admin_userReversationEdit',{'id': reservation.id})}}\">conclure location</a>
                            </td>
                            </td>
                        {% endif %}
                    {% endif %}
                </tr>
        {% endif %}
            {% endfor %}
            
        </tbody>
                    <td><a href=\"{{path('admin_userList')}}\" class=\"btn btn-warning\">Retour</a></td>
    </table>
{% endblock %}", "admin/reservationProgress.html.twig", "C:\\symfony\\locauto\\templates\\admin\\reservationProgress.html.twig");
    }
}
