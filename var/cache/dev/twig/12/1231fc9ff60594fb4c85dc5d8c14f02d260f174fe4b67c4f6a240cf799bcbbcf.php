<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* locauto/confirmReserve.html.twig */
class __TwigTemplate_c8394bb6cd07c9be820879534b745c52c7e806e7792374b389795f2b92b1f485 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "locauto/confirmReserve.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "locauto/confirmReserve.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "locauto/confirmReserve.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class =\"row\">
    <div class=\"col red\"></div>
    <div class=\"container col\">
        ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 7, $this->source); })()), 'form_start', ["attr" => ["id" => "confirm_form"]]);
        echo "
    <div class=\"formconfirm\">
         ";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 9, $this->source); })()), "categorie", [], "any", false, false, false, 9), 'row', ["label" => "choix categorie"]);
        echo "
          ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 10, $this->source); })()), "vehicle", [], "any", false, false, false, 10), 'row', ["label" => "choix vehicule"]);
        echo "
           ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 11, $this->source); })()), "startDate", [], "any", false, false, false, 11), 'row', ["label" => "Date de départ"]);
        echo "
            ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 12, $this->source); })()), "endDate", [], "any", false, false, false, 12), 'row', ["label" => "Date Retour"]);
        echo "
         ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 13, $this->source); })()), "takenInsurance", [], "any", false, false, false, 13), 'row', ["label" => "Voulez vous prendre l'assurance"]);
        echo "
    
    </div>

        <h1>le montant total de la reservation est de : ";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["estimatedPrice"]) || array_key_exists("estimatedPrice", $context) ? $context["estimatedPrice"] : (function () { throw new RuntimeError('Variable "estimatedPrice" does not exist.', 17, $this->source); })()), "html", null, true);
        echo "€</h1>
        <p>hors kilometrage</p>
        <h2>Remise promotionnel : ";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["remise"]) || array_key_exists("remise", $context) ? $context["remise"] : (function () { throw new RuntimeError('Variable "remise" does not exist.', 19, $this->source); })()), "html", null, true);
        echo "€</h2>
        <h2>Une caution de ";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["Guarantee"]) || array_key_exists("Guarantee", $context) ? $context["Guarantee"] : (function () { throw new RuntimeError('Variable "Guarantee" does not exist.', 20, $this->source); })()), "html", null, true);
        echo "€ vous sera demandé lors de la reception du vehicule !</h2>
        <h2>un email vous sera envoyé lors de la confirmation de votre reservation avec le recapitulatif de votr commande</h2>
        <h2>Confirmez-vous votre reservation ?</h2>
            <button type=\"submit\" class=\"btn btn-success\" name =\"return\">Retour</button>
            <button type=\"submit\" class=\"btn btn-success\" name =\"confirm\">Confirmer</button>
        ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 25, $this->source); })()), 'form_end');
        echo "
    </div>
    <div class=\"col red\"></div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "locauto/confirmReserve.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 25,  110 => 20,  106 => 19,  101 => 17,  94 => 13,  90 => 12,  86 => 11,  82 => 10,  78 => 9,  73 => 7,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<div class =\"row\">
    <div class=\"col red\"></div>
    <div class=\"container col\">
        {{ form_start(formReservation,{'attr': {'id': 'confirm_form'}}) }}
    <div class=\"formconfirm\">
         {{ form_row(formReservation.categorie,{'label':'choix categorie'}) }}
          {{ form_row(formReservation.vehicle,{'label':'choix vehicule'}) }}
           {{ form_row(formReservation.startDate,{'label':'Date de départ'}) }}
            {{ form_row(formReservation.endDate,{'label':'Date Retour'}) }}
         {{ form_row(formReservation.takenInsurance,{'label':'Voulez vous prendre l\\'assurance'}) }}
    
    </div>

        <h1>le montant total de la reservation est de : {{estimatedPrice}}€</h1>
        <p>hors kilometrage</p>
        <h2>Remise promotionnel : {{ remise }}€</h2>
        <h2>Une caution de {{Guarantee}}€ vous sera demandé lors de la reception du vehicule !</h2>
        <h2>un email vous sera envoyé lors de la confirmation de votre reservation avec le recapitulatif de votr commande</h2>
        <h2>Confirmez-vous votre reservation ?</h2>
            <button type=\"submit\" class=\"btn btn-success\" name =\"return\">Retour</button>
            <button type=\"submit\" class=\"btn btn-success\" name =\"confirm\">Confirmer</button>
        {{ form_end(formReservation) }}
    </div>
    <div class=\"col red\"></div>
</div>
{% endblock %}", "locauto/confirmReserve.html.twig", "C:\\symfony\\locauto\\templates\\locauto\\confirmReserve.html.twig");
    }
}
