<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* locauto/index.html.twig */
class __TwigTemplate_db3cfad51089ad8ff851f51ca3c593ea579c4916558f8b7238f78a7dd2005beb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "locauto/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "locauto/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "locauto/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Acceuil";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"container-fluid red\">
   ";
        // line 7
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "user", [], "any", false, false, false, 7)) {
            // line 8
            echo "        <h1>Réservez votre véhicule</h1>
        <div class=\"container\">";
            // line 9
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 10
                echo "          ";
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 10, $this->source); })()), 'form_start', ["attr" => ["id" => "confirm_form"]]);
                echo "
          ";
            } else {
                // line 12
                echo "          ";
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 12, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("confirm_reserve")]);
                echo "
        ";
            }
            // line 14
            echo "          <div class=\"row align-items-center\">
            <div class=\"row col FormReserve\">
               <div class=\" col\">
                ";
            // line 17
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 17, $this->source); })()), "categorie", [], "any", false, false, false, 17), 'row', ["label" => "choix categorie"]);
            echo "
              </div>
              ";
            // line 19
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 20
                echo "                <div class=\" col \">
                    ";
                // line 21
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 21, $this->source); })()), "vehicle", [], "any", false, false, false, 21), 'row', ["label" => "choix vehicule*"]);
                echo "  
                </div>
              ";
            }
            // line 24
            echo "              <div class=\" col formconfirm \">
                  ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 25, $this->source); })()), "vehicle", [], "any", false, false, false, 25), 'row', ["label" => "choix vehicule*"]);
            echo "  
              </div>
              <div class=\" col\">
                ";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 28, $this->source); })()), "startDate", [], "any", false, false, false, 28), 'row', ["label" => "Date de départ"]);
            echo "
              </div>
              <div class=\"col\">
                ";
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 31, $this->source); })()), "endDate", [], "any", false, false, false, 31), 'row', ["label" => "Date Retour"]);
            echo "
              </div>
              <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
                  ";
            // line 34
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 34, $this->source); })()), "takenInsurance", [], "any", false, false, false, 34), 'row', ["label" => "Voulez vous prendre l'assurance"]);
            echo "
              </div>
              <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
                <button type=\"submit\" class=\"btn btn-success\"> Reserver !</button>
              </div>
                <p>Un choix de véhicule selon la catégorie choisie vous sera proposé en agence</p>
                <p>le tarif week-end est automatiquement prise en compte pour une periode comprise du vendredi 16h au Dimanche 19h</p>
            </div>
            <div class=\"col\">
                <img src=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/eco.png"), "html", null, true);
            echo "\" width=\"400\" height=\"300\" >
            </div>
          </div>
            ";
            // line 46
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formReservation"]) || array_key_exists("formReservation", $context) ? $context["formReservation"] : (function () { throw new RuntimeError('Variable "formReservation" does not exist.', 46, $this->source); })()), 'form_end');
            echo "
        </div>
        ";
        } else {
            // line 49
            echo "        <div class=\"container-fluid red\">
          <h1>Vous devez vous connecter pour créer une reservation</h1>
        </div>
        ";
        }
        // line 53
        echo "
    </div>
    <div class=\"alert alert-primary\" role=\"alert\">
      promotion : 10% de réduction pour 3 jour de location.
    </div>
  <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
  ";
        // line 59
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 59, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 60
            echo "        <div class=\"col\">
          <div class=\"card mb-4 rounded-3 shadow-sm\">
            <div class=\"card-header py-3\">
              <h4 class=\"my-0 fw-normal\">";
            // line 63
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "label", [], "any", false, false, false, 63), "html", null, true);
            echo "</h4>
            </div>
            <div class=\"card-body\">
            <img src=\"";
            // line 66
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/eco.png"), "html", null, true);
            echo "\" width=\"300\" height=\"200\" >
              <h1 class=\"card-title pricing-card-title\">";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "priceday", [], "any", false, false, false, 67), "html", null, true);
            echo "€<small class=\"text-muted fw-light\">/jour</small></h1>
              <ul class=\"list-unstyled mt-3 mb-4\">
                <li>";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "priceWeekEnd", [], "any", false, false, false, 69), "html", null, true);
            echo "€ en Week-end</li>
                <li>";
            // line 70
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "priceInsurance", [], "any", false, false, false, 70), "html", null, true);
            echo "€ d'assurance</li>
                <li>";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "guarantee", [], "any", false, false, false, 71), "html", null, true);
            echo "€ de caution</li>
              </ul>
            </div>
          </div>
        </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "  </div>
  <h1 class=\"titleWeather\">Meteo de la semaine à ";
        // line 78
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["city"]) || array_key_exists("city", $context) ? $context["city"] : (function () { throw new RuntimeError('Variable "city" does not exist.', 78, $this->source); })()), "name", [], "any", false, false, false, 78), "html", null, true);
        echo "</h1>

<div class=\"row\">
    <div>
    <div class=\"row row-cols-12 row-cols-md-12 mb-12 text-center\">
      <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
            <h4 class=\"my-0 fw-normal\"></h4>
          </div>
          <div class=\"card-body\">
            <h1 class=\"card-title pricing-card-title\"> ";
        // line 89
        echo twig_escape_filter($this->env, (isset($context["today"]) || array_key_exists("today", $context) ? $context["today"] : (function () { throw new RuntimeError('Variable "today" does not exist.', 89, $this->source); })()), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 89, $this->source); })()), 0, [], "array", false, false, false, 89), "tmin", [], "any", false, false, false, 89), "html", null, true);
        echo "°C/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 89, $this->source); })()), 0, [], "array", false, false, false, 89), "tmax", [], "any", false, false, false, 89), "html", null, true);
        echo "°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max ";
        // line 91
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 91, $this->source); })()), 0, [], "array", false, false, false, 91), "rr1", [], "any", false, false, false, 91), "html", null, true);
        echo "</li>
              <li>precipitation min ";
        // line 92
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 92, $this->source); })()), 0, [], "array", false, false, false, 92), "rr10", [], "any", false, false, false, 92), "html", null, true);
        echo "</li>
            </ul>
          </div>
        </div>
      </div>
      <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">";
        // line 102
        echo twig_escape_filter($this->env, (isset($context["tomorrow"]) || array_key_exists("tomorrow", $context) ? $context["tomorrow"] : (function () { throw new RuntimeError('Variable "tomorrow" does not exist.', 102, $this->source); })()), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 102, $this->source); })()), 1, [], "array", false, false, false, 102), "tmin", [], "any", false, false, false, 102), "html", null, true);
        echo "°C/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 102, $this->source); })()), 1, [], "array", false, false, false, 102), "tmax", [], "any", false, false, false, 102), "html", null, true);
        echo "°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max ";
        // line 104
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 104, $this->source); })()), 1, [], "array", false, false, false, 104), "rr1", [], "any", false, false, false, 104), "html", null, true);
        echo "</li>
              <li>precipitation min ";
        // line 105
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 105, $this->source); })()), 1, [], "array", false, false, false, 105), "rr10", [], "any", false, false, false, 105), "html", null, true);
        echo "</li>
            </ul>
          </div>
        </div>
      </div>
      <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">";
        // line 115
        echo twig_escape_filter($this->env, (isset($context["afterTomorrow"]) || array_key_exists("afterTomorrow", $context) ? $context["afterTomorrow"] : (function () { throw new RuntimeError('Variable "afterTomorrow" does not exist.', 115, $this->source); })()), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 115, $this->source); })()), 2, [], "array", false, false, false, 115), "tmin", [], "any", false, false, false, 115), "html", null, true);
        echo "°C/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 115, $this->source); })()), 2, [], "array", false, false, false, 115), "tmax", [], "any", false, false, false, 115), "html", null, true);
        echo "°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max ";
        // line 117
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 117, $this->source); })()), 2, [], "array", false, false, false, 117), "rr1", [], "any", false, false, false, 117), "html", null, true);
        echo "</li>
              <li>precipitation min ";
        // line 118
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 118, $this->source); })()), 2, [], "array", false, false, false, 118), "rr10", [], "any", false, false, false, 118), "html", null, true);
        echo "</li>
            </ul>
          </div>
        </div>
    </div>
    <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">";
        // line 128
        echo twig_escape_filter($this->env, (isset($context["day4"]) || array_key_exists("day4", $context) ? $context["day4"] : (function () { throw new RuntimeError('Variable "day4" does not exist.', 128, $this->source); })()), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 128, $this->source); })()), 3, [], "array", false, false, false, 128), "tmin", [], "any", false, false, false, 128), "html", null, true);
        echo "°C/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 128, $this->source); })()), 3, [], "array", false, false, false, 128), "tmax", [], "any", false, false, false, 128), "html", null, true);
        echo "°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max ";
        // line 130
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 130, $this->source); })()), 3, [], "array", false, false, false, 130), "rr1", [], "any", false, false, false, 130), "html", null, true);
        echo "</li>
              <li>precipitation min ";
        // line 131
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 131, $this->source); })()), 3, [], "array", false, false, false, 131), "rr10", [], "any", false, false, false, 131), "html", null, true);
        echo "</li>
            </ul>
          </div>
        </div>
      </div>   <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">";
        // line 140
        echo twig_escape_filter($this->env, (isset($context["day5"]) || array_key_exists("day5", $context) ? $context["day5"] : (function () { throw new RuntimeError('Variable "day5" does not exist.', 140, $this->source); })()), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 140, $this->source); })()), 4, [], "array", false, false, false, 140), "tmin", [], "any", false, false, false, 140), "html", null, true);
        echo "°C/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 140, $this->source); })()), 4, [], "array", false, false, false, 140), "tmax", [], "any", false, false, false, 140), "html", null, true);
        echo "°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max ";
        // line 142
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 142, $this->source); })()), 4, [], "array", false, false, false, 142), "rr1", [], "any", false, false, false, 142), "html", null, true);
        echo "</li>
              <li>precipitation min ";
        // line 143
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 143, $this->source); })()), 4, [], "array", false, false, false, 143), "rr10", [], "any", false, false, false, 143), "html", null, true);
        echo "</li>
            </ul>
          </div>
        </div>
      </div>   <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">";
        // line 152
        echo twig_escape_filter($this->env, (isset($context["day6"]) || array_key_exists("day6", $context) ? $context["day6"] : (function () { throw new RuntimeError('Variable "day6" does not exist.', 152, $this->source); })()), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 152, $this->source); })()), 5, [], "array", false, false, false, 152), "tmin", [], "any", false, false, false, 152), "html", null, true);
        echo "°C/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 152, $this->source); })()), 5, [], "array", false, false, false, 152), "tmax", [], "any", false, false, false, 152), "html", null, true);
        echo "°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max ";
        // line 154
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 154, $this->source); })()), 5, [], "array", false, false, false, 154), "rr1", [], "any", false, false, false, 154), "html", null, true);
        echo "</li>
              <li>precipitation min ";
        // line 155
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 155, $this->source); })()), 5, [], "array", false, false, false, 155), "rr10", [], "any", false, false, false, 155), "html", null, true);
        echo "</li>
            </ul>
          </div>
        </div>
      </div>   <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">";
        // line 164
        echo twig_escape_filter($this->env, (isset($context["day7"]) || array_key_exists("day7", $context) ? $context["day7"] : (function () { throw new RuntimeError('Variable "day7" does not exist.', 164, $this->source); })()), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 164, $this->source); })()), 6, [], "array", false, false, false, 164), "tmin", [], "any", false, false, false, 164), "html", null, true);
        echo "°C/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 164, $this->source); })()), 6, [], "array", false, false, false, 164), "tmax", [], "any", false, false, false, 164), "html", null, true);
        echo "°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max ";
        // line 166
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 166, $this->source); })()), 6, [], "array", false, false, false, 166), "rr1", [], "any", false, false, false, 166), "html", null, true);
        echo "</li>
              <li>precipitation min ";
        // line 167
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["forecasts"]) || array_key_exists("forecasts", $context) ? $context["forecasts"] : (function () { throw new RuntimeError('Variable "forecasts" does not exist.', 167, $this->source); })()), 6, [], "array", false, false, false, 167), "rr10", [], "any", false, false, false, 167), "html", null, true);
        echo "</li>
            </ul>
          </div>
        </div>
      </div>
</div>

<div class=\"container\">

<form action=\"\" method=\"post\">
    <h2>Vous pouvez entrer une nouvelle adresse !</h2>
        <div class=\"mb-3\">
  <label for=\"exampleFormControlInput1\" class=\"form-label\">Adresse</label>
  <input type=\"text\" name=\"adresse\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"1 rue place de la république\">
</div>
<div class=\"mb-3\">
  <label for=\"exampleFormControlInput1\" class=\"form-label\">code postal</label>
  <input type=\"text\"  name=\"cp\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"54000\">
</div>
<div class=\"mb-3\">
  <label for=\"exampleFormControlInput1\"  class=\"form-label\">ville</label>
  <input type=\"text\" name=\"ville\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"Nancy\">
</div>
<div class=\"mb-3\">
  <label for=\"exampleFormControlInput1\" name=\"pays\" class=\"form-label\">pays</label>
  <input type=\"text\" name=\"pays\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"France\">
</div>
<input type=\"submit\" name=\"envoyer\" value=\"envoyer\">
        </form>
</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "locauto/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  417 => 167,  413 => 166,  404 => 164,  392 => 155,  388 => 154,  379 => 152,  367 => 143,  363 => 142,  354 => 140,  342 => 131,  338 => 130,  329 => 128,  316 => 118,  312 => 117,  303 => 115,  290 => 105,  286 => 104,  277 => 102,  264 => 92,  260 => 91,  251 => 89,  237 => 78,  234 => 77,  222 => 71,  218 => 70,  214 => 69,  209 => 67,  205 => 66,  199 => 63,  194 => 60,  190 => 59,  182 => 53,  176 => 49,  170 => 46,  164 => 43,  152 => 34,  146 => 31,  140 => 28,  134 => 25,  131 => 24,  125 => 21,  122 => 20,  120 => 19,  115 => 17,  110 => 14,  104 => 12,  98 => 10,  96 => 9,  93 => 8,  91 => 7,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Acceuil{% endblock %}

{% block body %}
    <div class=\"container-fluid red\">
   {% if app.user %}
        <h1>Réservez votre véhicule</h1>
        <div class=\"container\">{% if is_granted('ROLE_ADMIN') %}
          {{ form_start( formReservation,{'attr': {'id': 'confirm_form'}} ) }}
          {% else %}
          {{ form_start( formReservation,{'action': path('confirm_reserve')},{'attr': {'id': 'confirm_form'}} ) }}
        {% endif %}
          <div class=\"row align-items-center\">
            <div class=\"row col FormReserve\">
               <div class=\" col\">
                {{ form_row(formReservation.categorie,{'label':'choix categorie'}) }}
              </div>
              {% if is_granted('ROLE_ADMIN') %}
                <div class=\" col \">
                    {{ form_row(formReservation.vehicle,{'label':'choix vehicule*'}) }}  
                </div>
              {% endif %}
              <div class=\" col formconfirm \">
                  {{ form_row(formReservation.vehicle,{'label':'choix vehicule*'}) }}  
              </div>
              <div class=\" col\">
                {{ form_row(formReservation.startDate,{'label':'Date de départ'}) }}
              </div>
              <div class=\"col\">
                {{ form_row(formReservation.endDate,{'label':'Date Retour'}) }}
              </div>
              <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
                  {{ form_row(formReservation.takenInsurance,{'label':'Voulez vous prendre l\\'assurance'}) }}
              </div>
              <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
                <button type=\"submit\" class=\"btn btn-success\"> Reserver !</button>
              </div>
                <p>Un choix de véhicule selon la catégorie choisie vous sera proposé en agence</p>
                <p>le tarif week-end est automatiquement prise en compte pour une periode comprise du vendredi 16h au Dimanche 19h</p>
            </div>
            <div class=\"col\">
                <img src=\"{{ asset('build/eco.png') }}\" width=\"400\" height=\"300\" >
            </div>
          </div>
            {{ form_end(formReservation) }}
        </div>
        {% else %}
        <div class=\"container-fluid red\">
          <h1>Vous devez vous connecter pour créer une reservation</h1>
        </div>
        {% endif %}

    </div>
    <div class=\"alert alert-primary\" role=\"alert\">
      promotion : 10% de réduction pour 3 jour de location.
    </div>
  <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center\">
  {% for category in categories %}
        <div class=\"col\">
          <div class=\"card mb-4 rounded-3 shadow-sm\">
            <div class=\"card-header py-3\">
              <h4 class=\"my-0 fw-normal\">{{category.label}}</h4>
            </div>
            <div class=\"card-body\">
            <img src=\"{{ asset('build/eco.png') }}\" width=\"300\" height=\"200\" >
              <h1 class=\"card-title pricing-card-title\">{{category.priceday}}€<small class=\"text-muted fw-light\">/jour</small></h1>
              <ul class=\"list-unstyled mt-3 mb-4\">
                <li>{{category.priceWeekEnd}}€ en Week-end</li>
                <li>{{category.priceInsurance}}€ d'assurance</li>
                <li>{{category.guarantee}}€ de caution</li>
              </ul>
            </div>
          </div>
        </div>
  {% endfor %}
  </div>
  <h1 class=\"titleWeather\">Meteo de la semaine à {{city.name}}</h1>

<div class=\"row\">
    <div>
    <div class=\"row row-cols-12 row-cols-md-12 mb-12 text-center\">
      <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
            <h4 class=\"my-0 fw-normal\"></h4>
          </div>
          <div class=\"card-body\">
            <h1 class=\"card-title pricing-card-title\"> {{today}}: {{forecasts[0].tmin}}°C/{{forecasts[0].tmax}}°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max {{forecasts[0].rr1}}</li>
              <li>precipitation min {{forecasts[0].rr10}}</li>
            </ul>
          </div>
        </div>
      </div>
      <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">{{tomorrow}}: {{forecasts[1].tmin}}°C/{{forecasts[1].tmax}}°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max {{forecasts[1].rr1}}</li>
              <li>precipitation min {{forecasts[1].rr10}}</li>
            </ul>
          </div>
        </div>
      </div>
      <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">{{afterTomorrow}}: {{forecasts[2].tmin}}°C/{{forecasts[2].tmax}}°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max {{forecasts[2].rr1}}</li>
              <li>precipitation min {{forecasts[2].rr10}}</li>
            </ul>
          </div>
        </div>
    </div>
    <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">{{day4}}: {{forecasts[3].tmin}}°C/{{forecasts[3].tmax}}°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max {{forecasts[3].rr1}}</li>
              <li>precipitation min {{forecasts[3].rr10}}</li>
            </ul>
          </div>
        </div>
      </div>   <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">{{day5}}: {{forecasts[4].tmin}}°C/{{forecasts[4].tmax}}°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max {{forecasts[4].rr1}}</li>
              <li>precipitation min {{forecasts[4].rr10}}</li>
            </ul>
          </div>
        </div>
      </div>   <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">{{day6}}: {{forecasts[5].tmin}}°C/{{forecasts[5].tmax}}°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max {{forecasts[5].rr1}}</li>
              <li>precipitation min {{forecasts[5].rr10}}</li>
            </ul>
          </div>
        </div>
      </div>   <div class=\"col\">
        <div class=\"card mb-4 rounded-3 shadow-sm\">
          <div class=\"card-header py-3\">
          </div>
          <div class=\"card-body\">
          <h1 class=\"card-title pricing-card-title\">{{day7}}: {{forecasts[6].tmin}}°C/{{forecasts[6].tmax}}°C</small></h1>
            <ul class=\"list-unstyled mt-3 mb-4\">
              <li>precipitation max {{forecasts[6].rr1}}</li>
              <li>precipitation min {{forecasts[6].rr10}}</li>
            </ul>
          </div>
        </div>
      </div>
</div>

<div class=\"container\">

<form action=\"\" method=\"post\">
    <h2>Vous pouvez entrer une nouvelle adresse !</h2>
        <div class=\"mb-3\">
  <label for=\"exampleFormControlInput1\" class=\"form-label\">Adresse</label>
  <input type=\"text\" name=\"adresse\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"1 rue place de la république\">
</div>
<div class=\"mb-3\">
  <label for=\"exampleFormControlInput1\" class=\"form-label\">code postal</label>
  <input type=\"text\"  name=\"cp\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"54000\">
</div>
<div class=\"mb-3\">
  <label for=\"exampleFormControlInput1\"  class=\"form-label\">ville</label>
  <input type=\"text\" name=\"ville\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"Nancy\">
</div>
<div class=\"mb-3\">
  <label for=\"exampleFormControlInput1\" name=\"pays\" class=\"form-label\">pays</label>
  <input type=\"text\" name=\"pays\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"France\">
</div>
<input type=\"submit\" name=\"envoyer\" value=\"envoyer\">
        </form>
</div>
</div>
{% endblock %}
", "locauto/index.html.twig", "C:\\symfony\\locauto\\templates\\locauto\\index.html.twig");
    }
}
