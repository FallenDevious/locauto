<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/registration.html.twig */
class __TwigTemplate_c1b8ca5a696241f6ae048f62965cf75c7fdc5c967ab6a0022f81e2728488556f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/registration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/registration.html.twig"));

        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 2, $this->source); })()), [0 => "bootstrap_4_layout.html.twig"], true);
        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "security/registration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Inscription";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
    <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center \">
        <div class=\"row\">
                <div class=\" red\">
                <img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/luxe.png"), "html", null, true);
        echo "\"  width=\"400\" height=\"300\">
            </div>
                    <div class=\" red\">
                <img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/luxe.png"), "html", null, true);
        echo "\"  width=\"400\" height=\"300\">
            </div>
        </div>

        ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), 'form_start');
        echo "
        <h1>Inscription</h1>
        
        ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "username", [], "any", false, false, false, 21), 'row', ["label" => "Nom d'utilisateur", "attr" => ["placeholder" => "Nom d'utilisateur"]]);
        // line 22
        echo "
        ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "nameUser", [], "any", false, false, false, 23), 'row', ["label" => "Nom de Famille", "attr" => ["placeholder" => "Nom de Famille..."]]);
        // line 24
        echo "
        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 25, $this->source); })()), "firstNameUser", [], "any", false, false, false, 25), 'row', ["label" => "prenom", "attr" => ["placeholder" => "prenom..."]]);
        // line 26
        echo "
        ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), "email", [], "any", false, false, false, 27), 'row', ["label" => "email", "attr" => ["placeholder" => "Entrez votre adresse email..."]]);
        // line 28
        echo "
        ";
        // line 29
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 30
            echo "        <div class ='formconfirm'>
            ";
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "password", [], "any", false, false, false, 31), 'row', ["label" => "Mot de passe", "attr" => ["placeholder" => "Nouveau mot de passe..."], "value" => "{{passwordRandom}}"]);
            // line 32
            echo "
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "confirm_password", [], "any", false, false, false, 33), 'row', ["label" => "Corfirmation du mot de passe", "attr" => ["placeholder" => "Veuillez confirmer votre mot de passe..."], "value" => "{{passwordRandom}}"]);
            // line 34
            echo "
        </div>
            ";
        } else {
            // line 37
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 37, $this->source); })()), "password", [], "any", false, false, false, 37), 'row', ["label" => "Mot de passe", "attr" => ["placeholder" => "Nouveau mot de passe..."]]);
            // line 38
            echo "
            ";
            // line 39
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 39, $this->source); })()), "confirm_password", [], "any", false, false, false, 39), 'row', ["label" => "Corfirmation du mot de passe", "attr" => ["placeholder" => "Veuillez confirmer votre mot de passe..."]]);
            // line 40
            echo "
        ";
        }
        // line 42
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 42, $this->source); })()), "address", [], "any", false, false, false, 42), 'row', ["label" => "adresse", "attr" => ["placeholder" => "24 rue de la paix"]]);
        // line 43
        echo "
        ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 44, $this->source); })()), "zipCode", [], "any", false, false, false, 44), 'row', ["label" => "Code Postale", "attr" => ["placeholder" => "54360..."]]);
        // line 45
        echo "
        ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 46, $this->source); })()), "city", [], "any", false, false, false, 46), 'row', ["label" => "Ville", "attr" => ["placeholder" => "Paris"]]);
        // line 47
        echo "
        ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 48, $this->source); })()), "numberLicence", [], "any", false, false, false, 48), 'row', ["label" => "Nuéro de permis", "attr" => ["placeholder" => "911091204209"]]);
        // line 49
        echo "
        ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 50, $this->source); })()), "phone", [], "any", false, false, false, 50), 'row', ["label" => "Numéro de téléphone", "attr" => ["placeholder" => "0654987549"]]);
        // line 51
        echo "
        ";
        // line 52
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 52, $this->source); })()), "dateLicenseObtained", [], "any", false, false, false, 52), 'row', ["label" => "Date d'obtention du permis"]);
        echo "
        ";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 53, $this->source); })()), "expiryDateLicense", [], "any", false, false, false, 53), 'row', ["label" => "date d'expriration du permis"]);
        echo "

        <button type=\"submit\" class = \"btn btn-success\">Retour</button>
        <button type=\"submit\" class = \"btn btn-success\">Inscription</button>
        ";
        // line 57
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 57, $this->source); })()), 'form_end');
        echo "

        <div class=\"row\">
            <div class=\" red\">
                <img src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/moyen.png"), "html", null, true);
        echo "\"  width=\"400\" height=\"300\">
            </div>
            <div class=\" red\">
                <img src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/moyen.png"), "html", null, true);
        echo "\"  width=\"400\" height=\"300\">
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/registration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 64,  209 => 61,  202 => 57,  195 => 53,  191 => 52,  188 => 51,  186 => 50,  183 => 49,  181 => 48,  178 => 47,  176 => 46,  173 => 45,  171 => 44,  168 => 43,  165 => 42,  161 => 40,  159 => 39,  156 => 38,  153 => 37,  148 => 34,  146 => 33,  143 => 32,  141 => 31,  138 => 30,  136 => 29,  133 => 28,  131 => 27,  128 => 26,  126 => 25,  123 => 24,  121 => 23,  118 => 22,  116 => 21,  110 => 18,  103 => 14,  97 => 11,  91 => 7,  81 => 6,  62 => 4,  51 => 1,  49 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% form_theme form 'bootstrap_4_layout.html.twig' %}

{% block title %}Inscription{% endblock %}

{% block body %}

    <div class=\"row row-cols-1 row-cols-md-3 mb-3 text-center \">
        <div class=\"row\">
                <div class=\" red\">
                <img src=\"{{ asset('build/luxe.png') }}\"  width=\"400\" height=\"300\">
            </div>
                    <div class=\" red\">
                <img src=\"{{ asset('build/luxe.png') }}\"  width=\"400\" height=\"300\">
            </div>
        </div>

        {{ form_start(form)}}
        <h1>Inscription</h1>
        
        {{ form_row(form.username,{'label':'Nom d\\'utilisateur','attr':
        {'placeholder':'Nom d\\'utilisateur'}}) }}
        {{ form_row(form.nameUser,{'label':'Nom de Famille','attr':
        {'placeholder':'Nom de Famille...'}}) }}
        {{ form_row(form.firstNameUser,{'label':'prenom','attr':
        {'placeholder':'prenom...'}}) }}
        {{ form_row(form.email,{'label':'email','attr':
        {'placeholder':'Entrez votre adresse email...'}}) }}
        {% if is_granted('ROLE_ADMIN') %}
        <div class ='formconfirm'>
            {{ form_row(form.password,{'label':'Mot de passe','attr':
            {'placeholder':'Nouveau mot de passe...'},'value' :'{{passwordRandom}}'}) }}
            {{ form_row(form.confirm_password,{'label':'Corfirmation du mot de passe','attr':
            {'placeholder':'Veuillez confirmer votre mot de passe...'},'value' :'{{passwordRandom}}'})   }}
        </div>
            {% else %}
            {{ form_row(form.password,{'label':'Mot de passe','attr':
            {'placeholder':'Nouveau mot de passe...'}}) }}
            {{ form_row(form.confirm_password,{'label':'Corfirmation du mot de passe','attr':
            {'placeholder':'Veuillez confirmer votre mot de passe...'}})   }}
        {% endif %}
        {{ form_row(form.address,{'label':'adresse','attr':
        {'placeholder':'24 rue de la paix'}}) }}
        {{ form_row(form.zipCode,{'label':'Code Postale','attr':
        {'placeholder': '54360...'}}) }}
        {{ form_row(form.city,{'label':'Ville','attr':
        {'placeholder':'Paris'}}) }}
        {{ form_row(form.numberLicence,{'label':'Nuéro de permis','attr':
        {'placeholder':'911091204209'}}) }}
        {{ form_row(form.phone,{'label':'Numéro de téléphone','attr':
        {'placeholder':'0654987549'}}) }}
        {{ form_row(form.dateLicenseObtained,{'label':'Date d\\'obtention du permis'}) }}
        {{ form_row(form.expiryDateLicense,{'label':'date d\\'expriration du permis'}) }}

        <button type=\"submit\" class = \"btn btn-success\">Retour</button>
        <button type=\"submit\" class = \"btn btn-success\">Inscription</button>
        {{ form_end(form) }}

        <div class=\"row\">
            <div class=\" red\">
                <img src=\"{{ asset('build/moyen.png') }}\"  width=\"400\" height=\"300\">
            </div>
            <div class=\" red\">
                <img src=\"{{ asset('build/moyen.png') }}\"  width=\"400\" height=\"300\">
            </div>
        </div>
    </div>
{% endblock %}", "security/registration.html.twig", "C:\\symfony\\locauto\\templates\\security\\registration.html.twig");
    }
}
