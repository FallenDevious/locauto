<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/userReservations.html.twig */
class __TwigTemplate_5db8dcd8f5021b4bbf7061f5395dd35b3403e01a5e44a3e4a7015b6fdc9d580c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/userReservations.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/userReservations.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "admin/userReservations.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "reservations client";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <h1>Liste des reservation du client</h1>
    <table class=\"table\">
        <thead>
            <th scope=\"col\">numero de reservation</th>
            <th scope=\"col\">date de debut</th>
            <th scope=\"col\">date de fin</th>
            <th scope=\"col\">assurance prise</th>
            <th scope=\"col\">prix estimé</th>
            <th scope=\"col\">date retour du véhicule</th>
            <th scope=\"col\">km retour</th>
            <th scope=\"col\">prix total</th>
            <th scope=\"col\">identifiant client</th>
            <th scope=\"col\">Nom du client</th>
        </thead>
        <tbody>
            ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reservations"]) || array_key_exists("reservations", $context) ? $context["reservations"] : (function () { throw new RuntimeError('Variable "reservations" does not exist.', 21, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["reservation"]) {
            // line 22
            echo "                <tr>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "id", [], "any", false, false, false, 23), "html", null, true);
            echo "</td>
                    <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "startDate", [], "any", false, false, false, 24), "d/m/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "endDate", [], "any", false, false, false, 25), "d/m/Y"), "html", null, true);
            echo "</td>
                    
                    <td>
                        ";
            // line 28
            if ((twig_get_attribute($this->env, $this->source, $context["reservation"], "takenInsurance", [], "any", false, false, false, 28) == 1)) {
                // line 29
                echo "                            OUI
                        ";
            } else {
                // line 31
                echo "                        NON
                        ";
            }
            // line 32
            echo " 
                    </td>
                    <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "estimatedPrice", [], "any", false, false, false, 34), "html", null, true);
            echo "</td>
                    ";
            // line 35
            if (twig_get_attribute($this->env, $this->source, $context["reservation"], "returnDate", [], "any", false, false, false, 35)) {
                // line 36
                echo "                    <td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "returnDate", [], "any", false, false, false, 36), "d/m/Y"), "html", null, true);
                echo "</td>
                    ";
            } else {
                // line 38
                echo "                    <td>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "returnDate", [], "any", false, false, false, 38), "html", null, true);
                echo "</td>
                    ";
            }
            // line 40
            echo "                    <td>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "kmReturn", [], "any", false, false, false, 40), "html", null, true);
            echo "</td>
                    <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["reservation"], "priceReal", [], "any", false, false, false, 41), "html", null, true);
            echo "</td>
                    <td>";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reservation"], "user", [], "any", false, false, false, 42), "id", [], "any", false, false, false, 42), "html", null, true);
            echo "</td>
                    <td>";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reservation"], "user", [], "any", false, false, false, 43), "username", [], "any", false, false, false, 43), "html", null, true);
            echo "</td>
                   ";
            // line 44
            if ( !twig_get_attribute($this->env, $this->source, $context["reservation"], "kmReturn", [], "any", false, false, false, 44)) {
                // line 45
                echo "                        ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["reservation"], "vehicle", [], "any", false, false, false, 45), "rent", [], "any", false, false, false, 45) == false)) {
                    // line 46
                    echo "                            <td>
                            <a href=\"";
                    // line 47
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_confirmLoc", ["id" => twig_get_attribute($this->env, $this->source, $context["reservation"], "id", [], "any", false, false, false, 47)]), "html", null, true);
                    echo "\">confirmer location</a>
                            </td>
                            <td>
                            confirmer la location !
                            </td>
                            ";
                } else {
                    // line 53
                    echo "                            <td>
                                confirmé !
                            <td>
                                <a href=\"";
                    // line 56
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_userReversationEdit", ["id" => twig_get_attribute($this->env, $this->source, $context["reservation"], "id", [], "any", false, false, false, 56)]), "html", null, true);
                    echo "\">conclure location</a>
                            </td>
                            </td>
                        ";
                }
                // line 60
                echo "                    ";
            }
            // line 61
            echo "                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reservation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "        </tbody>
                    <td><a href=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_userList");
        echo "\" class=\"btn btn-warning\">Retour</a></td>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/userReservations.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 64,  214 => 63,  207 => 61,  204 => 60,  197 => 56,  192 => 53,  183 => 47,  180 => 46,  177 => 45,  175 => 44,  171 => 43,  167 => 42,  163 => 41,  158 => 40,  152 => 38,  146 => 36,  144 => 35,  140 => 34,  136 => 32,  132 => 31,  128 => 29,  126 => 28,  120 => 25,  116 => 24,  112 => 23,  109 => 22,  105 => 21,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block title %}reservations client{% endblock %}

{% block body %}
    <h1>Liste des reservation du client</h1>
    <table class=\"table\">
        <thead>
            <th scope=\"col\">numero de reservation</th>
            <th scope=\"col\">date de debut</th>
            <th scope=\"col\">date de fin</th>
            <th scope=\"col\">assurance prise</th>
            <th scope=\"col\">prix estimé</th>
            <th scope=\"col\">date retour du véhicule</th>
            <th scope=\"col\">km retour</th>
            <th scope=\"col\">prix total</th>
            <th scope=\"col\">identifiant client</th>
            <th scope=\"col\">Nom du client</th>
        </thead>
        <tbody>
            {% for reservation in reservations %}
                <tr>
                    <td>{{reservation.id}}</td>
                    <td>{{reservation.startDate | date('d/m/Y')}}</td>
                    <td>{{reservation.endDate | date('d/m/Y')}}</td>
                    
                    <td>
                        {% if reservation.takenInsurance ==  1 %}
                            OUI
                        {% else %}
                        NON
                        {% endif %} 
                    </td>
                    <td>{{reservation.estimatedPrice}}</td>
                    {% if reservation.returnDate %}
                    <td>{{reservation.returnDate | date('d/m/Y')}}</td>
                    {% else %}
                    <td>{{reservation.returnDate}}</td>
                    {% endif %}
                    <td>{{reservation.kmReturn}}</td>
                    <td>{{reservation.priceReal}}</td>
                    <td>{{reservation.user.id}}</td>
                    <td>{{reservation.user.username}}</td>
                   {% if not reservation.kmReturn %}
                        {% if reservation.vehicle.rent == false %}
                            <td>
                            <a href=\"{{path('admin_confirmLoc',{'id': reservation.id})}}\">confirmer location</a>
                            </td>
                            <td>
                            confirmer la location !
                            </td>
                            {% else %}
                            <td>
                                confirmé !
                            <td>
                                <a href=\"{{path('admin_userReversationEdit',{'id': reservation.id})}}\">conclure location</a>
                            </td>
                            </td>
                        {% endif %}
                    {% endif %}
                </tr>
            {% endfor %}
        </tbody>
                    <td><a href=\"{{path('admin_userList')}}\" class=\"btn btn-warning\">Retour</a></td>
    </table>
{% endblock %}", "admin/userReservations.html.twig", "C:\\symfony\\locauto\\templates\\admin\\userReservations.html.twig");
    }
}
