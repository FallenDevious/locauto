<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Category extends \App\Entity\Category implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'label', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'priceDay', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'priceKm', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'priceWeekEnd', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'priceInsurance', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'guarantee', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'nameCreatCat', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'dateCreatCat', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'vehicle'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'label', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'priceDay', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'priceKm', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'priceWeekEnd', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'priceInsurance', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'guarantee', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'nameCreatCat', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'dateCreatCat', '' . "\0" . 'App\\Entity\\Category' . "\0" . 'vehicle'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Category $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId(): ?int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getLabel(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLabel', []);

        return parent::getLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setLabel(string $label): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLabel', [$label]);

        return parent::setLabel($label);
    }

    /**
     * {@inheritDoc}
     */
    public function getPriceDay(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPriceDay', []);

        return parent::getPriceDay();
    }

    /**
     * {@inheritDoc}
     */
    public function setPriceDay(float $priceDay): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPriceDay', [$priceDay]);

        return parent::setPriceDay($priceDay);
    }

    /**
     * {@inheritDoc}
     */
    public function getPriceKm(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPriceKm', []);

        return parent::getPriceKm();
    }

    /**
     * {@inheritDoc}
     */
    public function setPriceKm(float $priceKm): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPriceKm', [$priceKm]);

        return parent::setPriceKm($priceKm);
    }

    /**
     * {@inheritDoc}
     */
    public function getPriceWeekEnd(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPriceWeekEnd', []);

        return parent::getPriceWeekEnd();
    }

    /**
     * {@inheritDoc}
     */
    public function setPriceWeekEnd(float $priceWeekEnd): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPriceWeekEnd', [$priceWeekEnd]);

        return parent::setPriceWeekEnd($priceWeekEnd);
    }

    /**
     * {@inheritDoc}
     */
    public function getPriceInsurance(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPriceInsurance', []);

        return parent::getPriceInsurance();
    }

    /**
     * {@inheritDoc}
     */
    public function setPriceInsurance(float $priceInsurance): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPriceInsurance', [$priceInsurance]);

        return parent::setPriceInsurance($priceInsurance);
    }

    /**
     * {@inheritDoc}
     */
    public function getGuarantee(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getGuarantee', []);

        return parent::getGuarantee();
    }

    /**
     * {@inheritDoc}
     */
    public function setGuarantee(float $guarantee): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setGuarantee', [$guarantee]);

        return parent::setGuarantee($guarantee);
    }

    /**
     * {@inheritDoc}
     */
    public function getNameCreatCat(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNameCreatCat', []);

        return parent::getNameCreatCat();
    }

    /**
     * {@inheritDoc}
     */
    public function setNameCreatCat(string $nameCreatCat): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNameCreatCat', [$nameCreatCat]);

        return parent::setNameCreatCat($nameCreatCat);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateCreatCat(): ?\DateTimeInterface
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateCreatCat', []);

        return parent::getDateCreatCat();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateCreatCat(\DateTimeInterface $dateCreatCat): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateCreatCat', [$dateCreatCat]);

        return parent::setDateCreatCat($dateCreatCat);
    }

    /**
     * {@inheritDoc}
     */
    public function getVehicle(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getVehicle', []);

        return parent::getVehicle();
    }

    /**
     * {@inheritDoc}
     */
    public function addVehicle(\App\Entity\Vehicle $vehicle): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addVehicle', [$vehicle]);

        return parent::addVehicle($vehicle);
    }

    /**
     * {@inheritDoc}
     */
    public function removeVehicle(\App\Entity\Vehicle $vehicle): \App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeVehicle', [$vehicle]);

        return parent::removeVehicle($vehicle);
    }

}
