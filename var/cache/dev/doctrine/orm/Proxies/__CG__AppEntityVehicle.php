<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Vehicle extends \App\Entity\Vehicle implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'model', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'brand', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'datePurchase', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'mileage', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'fuel', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'licensePlate', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'nameCreat', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'dateCreat', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'color', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'category', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'typeCarbu', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'agency', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'reserves', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'refueling', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'rent'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'model', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'brand', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'datePurchase', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'mileage', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'fuel', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'licensePlate', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'nameCreat', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'dateCreat', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'color', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'category', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'typeCarbu', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'agency', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'reserves', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'refueling', '' . "\0" . 'App\\Entity\\Vehicle' . "\0" . 'rent'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Vehicle $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId(): ?int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getModel(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getModel', []);

        return parent::getModel();
    }

    /**
     * {@inheritDoc}
     */
    public function setModel(string $model): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setModel', [$model]);

        return parent::setModel($model);
    }

    /**
     * {@inheritDoc}
     */
    public function getBrand(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBrand', []);

        return parent::getBrand();
    }

    /**
     * {@inheritDoc}
     */
    public function setBrand(string $brand): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBrand', [$brand]);

        return parent::setBrand($brand);
    }

    /**
     * {@inheritDoc}
     */
    public function getDatePurchase(): ?\DateTimeInterface
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDatePurchase', []);

        return parent::getDatePurchase();
    }

    /**
     * {@inheritDoc}
     */
    public function setDatePurchase(\DateTimeInterface $datePurchase): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDatePurchase', [$datePurchase]);

        return parent::setDatePurchase($datePurchase);
    }

    /**
     * {@inheritDoc}
     */
    public function getMileage(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMileage', []);

        return parent::getMileage();
    }

    /**
     * {@inheritDoc}
     */
    public function setMileage(float $mileage): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMileage', [$mileage]);

        return parent::setMileage($mileage);
    }

    /**
     * {@inheritDoc}
     */
    public function getFuel(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFuel', []);

        return parent::getFuel();
    }

    /**
     * {@inheritDoc}
     */
    public function setFuel(?float $fuel): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFuel', [$fuel]);

        return parent::setFuel($fuel);
    }

    /**
     * {@inheritDoc}
     */
    public function getLicensePlate(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLicensePlate', []);

        return parent::getLicensePlate();
    }

    /**
     * {@inheritDoc}
     */
    public function setLicensePlate(string $licensePlate): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLicensePlate', [$licensePlate]);

        return parent::setLicensePlate($licensePlate);
    }

    /**
     * {@inheritDoc}
     */
    public function getNameCreat(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNameCreat', []);

        return parent::getNameCreat();
    }

    /**
     * {@inheritDoc}
     */
    public function setNameCreat(string $nameCreat): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNameCreat', [$nameCreat]);

        return parent::setNameCreat($nameCreat);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateCreat(): ?\DateTimeInterface
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateCreat', []);

        return parent::getDateCreat();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateCreat(\DateTimeInterface $dateCreat): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateCreat', [$dateCreat]);

        return parent::setDateCreat($dateCreat);
    }

    /**
     * {@inheritDoc}
     */
    public function getColor(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getColor', []);

        return parent::getColor();
    }

    /**
     * {@inheritDoc}
     */
    public function setColor(string $color): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setColor', [$color]);

        return parent::setColor($color);
    }

    /**
     * {@inheritDoc}
     */
    public function getCategory(): ?\App\Entity\Category
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCategory', []);

        return parent::getCategory();
    }

    /**
     * {@inheritDoc}
     */
    public function setCategory(?\App\Entity\Category $category): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCategory', [$category]);

        return parent::setCategory($category);
    }

    /**
     * {@inheritDoc}
     */
    public function getTypeCarbu(): ?\App\Entity\TypeCarbu
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTypeCarbu', []);

        return parent::getTypeCarbu();
    }

    /**
     * {@inheritDoc}
     */
    public function setTypeCarbu(?\App\Entity\TypeCarbu $typeCarbu): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTypeCarbu', [$typeCarbu]);

        return parent::setTypeCarbu($typeCarbu);
    }

    /**
     * {@inheritDoc}
     */
    public function getAgency(): ?\App\Entity\Agency
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAgency', []);

        return parent::getAgency();
    }

    /**
     * {@inheritDoc}
     */
    public function setAgency(?\App\Entity\Agency $agency): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAgency', [$agency]);

        return parent::setAgency($agency);
    }

    /**
     * {@inheritDoc}
     */
    public function getRefueling(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRefueling', []);

        return parent::getRefueling();
    }

    /**
     * {@inheritDoc}
     */
    public function setRefueling(float $refueling): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRefueling', [$refueling]);

        return parent::setRefueling($refueling);
    }

    /**
     * {@inheritDoc}
     */
    public function getRent(): ?bool
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRent', []);

        return parent::getRent();
    }

    /**
     * {@inheritDoc}
     */
    public function setRent(bool $rent): \App\Entity\Vehicle
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRent', [$rent]);

        return parent::setRent($rent);
    }

}
