<?php

// This file has been auto-generated by the Symfony Routing Component.

return [
    '_preview_error' => [['code', '_format'], ['_controller' => 'error_controller::preview', '_format' => 'html'], ['code' => '\\d+'], [['variable', '.', '[^/]++', '_format'], ['variable', '/', '\\d+', 'code'], ['text', '/_error']], [], []],
    '_wdt' => [['token'], ['_controller' => 'web_profiler.controller.profiler::toolbarAction'], [], [['variable', '/', '[^/]++', 'token'], ['text', '/_wdt']], [], []],
    '_profiler_home' => [[], ['_controller' => 'web_profiler.controller.profiler::homeAction'], [], [['text', '/_profiler/']], [], []],
    '_profiler_search' => [[], ['_controller' => 'web_profiler.controller.profiler::searchAction'], [], [['text', '/_profiler/search']], [], []],
    '_profiler_search_bar' => [[], ['_controller' => 'web_profiler.controller.profiler::searchBarAction'], [], [['text', '/_profiler/search_bar']], [], []],
    '_profiler_phpinfo' => [[], ['_controller' => 'web_profiler.controller.profiler::phpinfoAction'], [], [['text', '/_profiler/phpinfo']], [], []],
    '_profiler_search_results' => [['token'], ['_controller' => 'web_profiler.controller.profiler::searchResultsAction'], [], [['text', '/search/results'], ['variable', '/', '[^/]++', 'token'], ['text', '/_profiler']], [], []],
    '_profiler_open_file' => [[], ['_controller' => 'web_profiler.controller.profiler::openAction'], [], [['text', '/_profiler/open']], [], []],
    '_profiler' => [['token'], ['_controller' => 'web_profiler.controller.profiler::panelAction'], [], [['variable', '/', '[^/]++', 'token'], ['text', '/_profiler']], [], []],
    '_profiler_router' => [['token'], ['_controller' => 'web_profiler.controller.router::panelAction'], [], [['text', '/router'], ['variable', '/', '[^/]++', 'token'], ['text', '/_profiler']], [], []],
    '_profiler_exception' => [['token'], ['_controller' => 'web_profiler.controller.exception_panel::body'], [], [['text', '/exception'], ['variable', '/', '[^/]++', 'token'], ['text', '/_profiler']], [], []],
    '_profiler_exception_css' => [['token'], ['_controller' => 'web_profiler.controller.exception_panel::stylesheet'], [], [['text', '/exception.css'], ['variable', '/', '[^/]++', 'token'], ['text', '/_profiler']], [], []],
    'admin_index' => [[], ['_controller' => 'App\\Controller\\AdminController::index'], [], [['text', '/admin/']], [], []],
    'admin_vehicle' => [[], ['_controller' => 'App\\Controller\\AdminController::vehicleList'], [], [['text', '/admin/vehicle']], [], []],
    'admin_creat_vehicle' => [[], ['_controller' => 'App\\Controller\\AdminController::creatVehicle'], [], [['text', '/admin/creatvehicle']], [], []],
    'admin_resell_vehicle' => [[], ['_controller' => 'App\\Controller\\AdminController::resellVehicle'], [], [['text', '/admin/resellvehicle']], [], []],
    'admin_planning' => [['id'], ['_controller' => 'App\\Controller\\AdminController::planning'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/admin/planning']], [], []],
    'admin_userList' => [[], ['_controller' => 'App\\Controller\\AdminController::userlist'], [], [['text', '/admin/userList']], [], []],
    'admin_userReversation' => [['id'], ['_controller' => 'App\\Controller\\AdminController::userReservation'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/admin/userReversation']], [], []],
    'admin_userReversationEdit' => [['id'], ['_controller' => 'App\\Controller\\AdminController::userReservationEdit'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/admin/userReversationEdit']], [], []],
    'admin_reservation_progress' => [[], ['_controller' => 'App\\Controller\\AdminController::reservationProgress'], [], [['text', '/admin/reservation/progess']], [], []],
    'admin_confirmLoc' => [['id'], ['_controller' => 'App\\Controller\\AdminController::confirmLoc'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/admin/ConfirmLoc']], [], []],
    'admin_confirmLoc2' => [['id'], ['_controller' => 'App\\Controller\\AdminController::confirmLoc2'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/admin/ConfirmLoc2']], [], []],
    'admin_reservation_admin' => [['id'], ['_controller' => 'App\\Controller\\AdminController::reservationAdmin'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/admin/reservation']], [], []],
    'locauto' => [[], ['_controller' => 'App\\Controller\\LocautoController::index'], [], [['text', '/']], [], []],
    'confirm_reserve' => [[], ['_controller' => 'App\\Controller\\LocautoController::confirm'], [], [['text', '/confirmReserve']], [], []],
    'security_registration' => [[], ['_controller' => 'App\\Controller\\SecurityController::registration'], [], [['text', '/inscription']], [], []],
    'security_login' => [[], ['_controller' => 'App\\Controller\\SecurityController::login'], [], [['text', '/connexion']], [], []],
    'security_logout' => [[], ['_controller' => 'App\\Controller\\SecurityController::logout'], [], [['text', '/deconnexion']], [], []],
];
