<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin_index', '_controller' => 'App\\Controller\\AdminController::index'], null, null, null, true, false, null]],
        '/admin/vehicle' => [[['_route' => 'admin_vehicle', '_controller' => 'App\\Controller\\AdminController::vehicleList'], null, null, null, false, false, null]],
        '/admin/creatvehicle' => [[['_route' => 'admin_creat_vehicle', '_controller' => 'App\\Controller\\AdminController::creatVehicle'], null, null, null, false, false, null]],
        '/admin/resellvehicle' => [[['_route' => 'admin_resell_vehicle', '_controller' => 'App\\Controller\\AdminController::resellVehicle'], null, null, null, false, false, null]],
        '/admin/userList' => [[['_route' => 'admin_userList', '_controller' => 'App\\Controller\\AdminController::userlist'], null, null, null, false, false, null]],
        '/admin/reservation/progess' => [[['_route' => 'admin_reservation_progress', '_controller' => 'App\\Controller\\AdminController::reservationProgress'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'locauto', '_controller' => 'App\\Controller\\LocautoController::index'], null, null, null, false, false, null]],
        '/confirmReserve' => [[['_route' => 'confirm_reserve', '_controller' => 'App\\Controller\\LocautoController::confirm'], null, null, null, false, false, null]],
        '/inscription' => [[['_route' => 'security_registration', '_controller' => 'App\\Controller\\SecurityController::registration'], null, null, null, false, false, null]],
        '/connexion' => [[['_route' => 'security_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/deconnexion' => [[['_route' => 'security_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/admin/(?'
                    .'|planning/([^/]++)(*:196)'
                    .'|userReversation(?'
                        .'|/([^/]++)(*:231)'
                        .'|Edit/([^/]++)(*:252)'
                    .')'
                    .'|ConfirmLoc(?'
                        .'|/([^/]++)(*:283)'
                        .'|2/([^/]++)(*:301)'
                    .')'
                    .'|reservation/([^/]++)(*:330)'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        196 => [[['_route' => 'admin_planning', '_controller' => 'App\\Controller\\AdminController::planning'], ['id'], null, null, false, true, null]],
        231 => [[['_route' => 'admin_userReversation', '_controller' => 'App\\Controller\\AdminController::userReservation'], ['id'], null, null, false, true, null]],
        252 => [[['_route' => 'admin_userReversationEdit', '_controller' => 'App\\Controller\\AdminController::userReservationEdit'], ['id'], null, null, false, true, null]],
        283 => [[['_route' => 'admin_confirmLoc', '_controller' => 'App\\Controller\\AdminController::confirmLoc'], ['id'], null, null, false, true, null]],
        301 => [[['_route' => 'admin_confirmLoc2', '_controller' => 'App\\Controller\\AdminController::confirmLoc2'], ['id'], null, null, false, true, null]],
        330 => [
            [['_route' => 'admin_reservation_admin', '_controller' => 'App\\Controller\\AdminController::reservationAdmin'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
