<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '2762323ef1f6d032ff05547a88a31e2414b5e201',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '2762323ef1f6d032ff05547a88a31e2414b5e201',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.1',
      'version' => '1.11.99.1',
      'aliases' => 
      array (
      ),
      'reference' => '7413f0b55a051e89485c5cb9f765fe24bb02a7b6',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b17c5014ef81d212ac539f07a1001832df1b6d3b',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9c1b59eba5a08ca2770a76eddb88922f504e8e0',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.7',
      'version' => '1.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f8b799269a1a472457bd1a41b4f379d4cfba4a',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a036d90c303f3163b5be8b8fde9b6755b2be4a3a',
    ),
    'doctrine/data-fixtures' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '51d3d4880d28951fff42a635a2389f8c63baddc5',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '2.13.1',
      'version' => '2.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c800380457948e65bbd30ba92cc17cda108bf8c9',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => 'v0.5.3',
      'version' => '0.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a08bc3b4d8567cdff05e89b272ba1e06e9d71c21',
    ),
    'doctrine/doctrine-fixtures-bundle' => 
    array (
      'pretty_version' => '3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '870189619a7770f468ffb0b80925302e065a3b34',
    ),
    'doctrine/doctrine-migrations-bundle' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '91f0a5e2356029575f3038432cc188b12f9d5da5',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'doctrine/migrations' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c2780df6b58998f411e64973cfa464ba0a06e00',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => '2.8.4',
      'version' => '2.8.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a588555ecd837b8d7e89355d9a13902e54d529c7',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9899c16934053880876b920a3b8b02ed2337ac1d',
    ),
    'doctrine/sql-formatter' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '56070bebac6e77230ed7d306ad13528e60732871',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c81f18a3efb941d8c4d2e025f6183b5c6d697307',
    ),
    'friendsofphp/proxy-manager-lts' => 
    array (
      'pretty_version' => 'v1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '121af47c9aee9c03031bdeca3fac0540f59aa5c3',
    ),
    'h4cc/wkhtmltoimage-amd64' => 
    array (
      'pretty_version' => '0.12.4',
      'version' => '0.12.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4e33f635207af89a704205b8902fb5715ca88be',
    ),
    'h4cc/wkhtmltopdf-amd64' => 
    array (
      'pretty_version' => '0.12.4',
      'version' => '0.12.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '4e2ab2d032a5d7fbe2a741de8b10b8989523c95b',
    ),
    'knplabs/knp-snappy' => 
    array (
      'pretty_version' => 'v1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7bac60fb729147b7ccd8532c07df3f52a4afa8a4',
    ),
    'knplabs/knp-snappy-bundle' => 
    array (
      'pretty_version' => 'v1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f81887b0379a2731b869289bf3d880f34dfd423',
    ),
    'laminas/laminas-code' => 
    array (
      'pretty_version' => '4.2.1',
      'version' => '4.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a1e7f8b6467ac7f277b8e027e8537fa13664a8d8',
    ),
    'laminas/laminas-eventmanager' => 
    array (
      'pretty_version' => '3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '966c859b67867b179fde1eff0cd38df51472ce4a',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6cccbddfcfc742eb02158d6137ca5687d92cee32',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2209ddd84e7ef1256b7af205d0717fb62cfc9c33',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.4',
      'version' => '4.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6d052fc58cb876152f89f532b95a8d7907e7f0e',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'ocramius/proxy-manager' => 
    array (
      'replaced' => 
      array (
        0 => '^2.1',
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'replaced' => 
      array (
        0 => '2.*',
      ),
    ),
    'php-http/async-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '213f9dbc5b9bfbc4f8db86d2838dc968752ce13b',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/link' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '846c25f58a1f02b93a00f2404e3626b6bf9b7807',
    ),
    'psr/link-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'sensio/framework-extra-bundle' => 
    array (
      'pretty_version' => 'v5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '430d14c01836b77c28092883d195a43ce413ee32',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.2.7',
      'version' => '6.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '15f7faf8508e04471f666633addacf54c0ab5933',
    ),
    'symfony/asset' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f2204a526c34945b1614cde033692983b6102eb8',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfa8d92f95294747e3abc04969efee51ed374424',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ff54be3f3eb1ce09643692f0c309b1b27bc992',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0446463729b89dd4fa62e9aeecc80287323615d',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '98606c6fa1a8f55ff964ccdd704275bf5b9f71b3',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ba4560dbbb9fcf5ae28b61f71f49c678086cf23',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f907d3e53ecb2a5fad8609eb2f30525287a734c8',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '157bbec4fd773bae53c5483c50951a5530a2cc16',
    ),
    'symfony/debug-bundle' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '1e136a4c6d8c2364b77e31c5bf124660cff6d084',
    ),
    'symfony/dependency-injection' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b5f97557faa48ead4671bc311cfca423d476e93e',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/doctrine-bridge' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e643bddb38277b4a1c2973d1489768c6e6c0db80',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be133557f1b0e6672367325b508e65da5513a311',
    ),
    'symfony/dotenv' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '4952e5ce9e6df3d737b9e9c337bddf781180a213',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '48e81a375525872e788c2418430f54150d935810',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c352647244bd376bf7d31efbd5401f13f50dad0c',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '84e23fdcd2517bf37aecbd16967e83f0caee25a7',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6b2c711e4d4dcba4db7b36a8a1835b0720d07fe',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '940826c465be2690c9fae91b2793481e5cbd6834',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '2543795ab1570df588b9bbd31e1a2bd7037b94f6',
    ),
    'symfony/flex' => 
    array (
      'pretty_version' => 'v1.12.2',
      'version' => '1.12.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e472606b4b3173564f0edbca8f5d32b52fc4f2c9',
    ),
    'symfony/form' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ced5b787916fb8a64819d63a4bcf7ddda46791c',
    ),
    'symfony/framework-bundle' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc72fbb0f9a69d2eb5d94cc8c231176265db680a',
    ),
    'symfony/http-client' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '911177e186b82e5b9a9f41c13af53699b6745657',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e82f6084d7cae521a75ef2cb5c9457bbda785f4',
    ),
    'symfony/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1|2.0',
      ),
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '02d968647fe61b2f419a8dc70c468a9d30a48d3a',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '0248214120d00c5f44f1cd5d9ad65f0b38459333',
    ),
    'symfony/inflector' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '9455097d23776a4a10c817d903271bc1ce7596ff',
    ),
    'symfony/intl' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe19cafd0ff661c2143c8717bb1dca0457794c1e',
    ),
    'symfony/mailer' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '76b64a4105634d89e1f7ee7f75851740fe8fe66b',
    ),
    'symfony/maker-bundle' => 
    array (
      'pretty_version' => 'v1.30.2',
      'version' => '1.30.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a395a85aa4ded6c1fa3da118d60329b64b6c2acd',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '50d7a1d569edad1f1321c59123c4c322c8daff7c',
    ),
    'symfony/monolog-bridge' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '3741314b95e8d0c11a485dce562898f5f67f455c',
    ),
    'symfony/monolog-bundle' => 
    array (
      'pretty_version' => 'v3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4054b2e940a25195ae15f0a49ab0c51718922eb4',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd8c6a2778d5f8b5e8fc4b7abdf126790b5d5095',
    ),
    'symfony/phpunit-bridge' => 
    array (
      'pretty_version' => 'v5.2.6',
      'version' => '5.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f2f94fd78379cdcdef09dd5025af791301913968',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-iconv' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af1842919c7e7364aaaa2798b29839e3ba168588',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2d63434d922daf7da8dd863e7907e67ee3031483',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '43a0283138253ed1d48d352ab6d0bdb3f809f248',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php56' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php70' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php71' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc6e6f9b39fe8075b3dabfbaf5b5f645ae1340c9',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a678b42e92f86eca04b7fa4c0f6f19d097fb69e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e950b6366d4da90292c2e7fa820b3c1842b965a',
    ),
    'symfony/property-access' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '94a1d9837396c71a0d8c31686c16693a15651622',
    ),
    'symfony/property-info' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '67845c335482cea0dd52497ae1314ce7a4978c74',
    ),
    'symfony/proxy-manager-bridge' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '811a39770b21f05bea9a737568074be4f02e7733',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '69919991c845b34626664ddc9b3aef9d09d2a5df',
    ),
    'symfony/security-bundle' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '607dcdb60ef74d63fbeb86549c52075f040ae4cc',
    ),
    'symfony/security-core' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '19a7caa988be4f013669a057861a1d2a3eacbbf3',
    ),
    'symfony/security-csrf' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '6864087a9c20eb4bb4063fc2f36954cec0ce28a6',
    ),
    'symfony/security-guard' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '20f522ada1eefb7c2f90cb83dcc76abb160c782f',
    ),
    'symfony/security-http' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c5546b762376e4d9a806b08bf4495b2633573ff8',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '9fa36329a06282e1fc856b84f645472a410c3922',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/service-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c5572f6494fc20668a73b77684d8dc77e534d8cf',
    ),
    'symfony/swiftmailer-bundle' => 
    array (
      'pretty_version' => 'v3.5.2',
      'version' => '3.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b72355549f02823a2209180f9c035e46ca3f178',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb8f5428cc3b40d6dffe303b195b084f1c5fbd14',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c812666f3e91db75385749fe219c5e494c7f95',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/twig-bridge' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5d0492a38c5325d9c322d406dbe95bc26fc530d',
    ),
    'symfony/twig-bundle' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cee73b45e3bd963a0ab4184f1041dcdc85b6e86',
    ),
    'symfony/validator' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c00da06b82b8591548f52b4d6aad0faa0985843e',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '0da0e174f728996f5d5072d6a9f0a42259dbc806',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v4.4.20',
      'version' => '4.4.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '3a3ea598bba6901d20b58c2579f68700089244ed',
    ),
    'symfony/web-link' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '565789cb9293a71a92410f50e663b91c0c4c0eac',
    ),
    'symfony/web-profiler-bundle' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bd848a0c0f3e7229e329adeea10e8945f70cb4c9',
    ),
    'symfony/web-server-bundle' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a3a7b8044cecd8df158984e05fea122d4f668a9f',
    ),
    'symfony/webpack-encore-bundle' => 
    array (
      'pretty_version' => 'v1.11.2',
      'version' => '1.11.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f282fb17ffa4839ba491eb7e3f5ffdd40c86f969',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.4.21',
      'version' => '4.4.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '3871c720871029f008928244e56cf43497da7e9d',
    ),
    'twig/extra-bundle' => 
    array (
      'pretty_version' => 'v3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2d27a86c3f47859eb07808fa7c8679d30fcbdde',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1f3b7e2c06cc05d42936a8ad508ff1db7975cdc5',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
    'zendframework/zend-code' => 
    array (
      'replaced' => 
      array (
        0 => '4.2.1',
      ),
    ),
    'zendframework/zend-eventmanager' => 
    array (
      'replaced' => 
      array (
        0 => '^3.2.1',
      ),
    ),
  ),
);
