/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

import { Tooltip, Toast, Popover } from 'bootstrap';

// start the Stimulus application
import './bootstrap';

 // loads the jquery package from node_modules
 var $ = require('jquery');

 require('bootstrap');

 //script permettant de d'affiché les voitures par rapport a la catégorie sans devoir soummettre le formulaire
$(document).on('change', '#reserve_categorie', '#reserve_vehicle', function () {
    let $field = $(this)
    let $categoryField = $('#reserve_categorie')
    let $form = $field.closest('formReservation')
    // Les données à envoyer en Ajax
    let data = {}
    data[$categoryField.attr('name')] = $categoryField.val()
    // On soumet les données
    $.post($form.attr('action'), data).then(function (data) {
      // On récupère le nouveau <select>
      let $input = $(data).find('#reserve_vehicle')

      // On remplace notre <select> actuel
      $('#reserve_vehicle').replaceWith($input)
    })
  })
