<?php

namespace App\Repository;

use App\Entity\TypeCarbu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeCarbu|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeCarbu|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeCarbu[]    findAll()
 * @method TypeCarbu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeCarbuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeCarbu::class);
    }

    // /**
    //  * @return TypeCarbu[] Returns an array of TypeCarbu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeCarbu
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
