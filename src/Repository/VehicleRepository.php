<?php

namespace App\Repository;

use DateInterval;
use App\Entity\Vehicle;
use App\Entity\Category;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Vehicle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vehicle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vehicle[]    findAll()
 * @method Vehicle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vehicle::class);
    }

    // /**
    //  * @return Vehicle[] Returns an array of Vehicle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vehicle
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

        /**
     * @return Vehicles[]
     */
    public function findResell(): array
    {
        $entityManager = $this->getEntityManager();
        $date1 = new \Datetime();
        $date2 = new \Datetime();
        $date1->sub(new DateInterval('P2Y'));
        $query = $entityManager->createQuery(
            'SELECT v FROM App\Entity\Vehicle v WHERE v.datePurchase NOT BETWEEN  :date1 and :date2'
        )->setParameter('date1', $date1)->setParameter('date2', $date2);

        return $query->getResult();
    }

      /**
     * @return Vehicles[]
     */
    public function findVehicleNotRent(Category $category)
{
    $entityManager = $this->getEntityManager();
    $date1 = new \Datetime();
    $date2 = new \Datetime();
    $date1->sub(new DateInterval('P2Y'));
        $query = $entityManager->createQuery(
            'SELECT v FROM App\Entity\Vehicle v WHERE v.rent = :rent and v.category = :category and v.datePurchase BETWEEN  :date1 and :date2 '
        )->setParameter('rent',false)
        ->setParameter('category', $category)
        ->setParameter('date1', $date1)
        ->setParameter('date2', $date2);

        return $query->getResult();
}
}
