<?php

namespace App\Entity;

use App\Repository\AgencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AgencyRepository::class)
 */
class Agency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nameAgency;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $zipCodeAgency;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cityAgency;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameStreetAgency;

    /**
     * @ORM\OneToMany(targetEntity=Vehicle::class, mappedBy="agency")
     */
    private $vehicle;

    public function __construct()
    {
        $this->vehicle = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameAgency(): ?string
    {
        return $this->nameAgency;
    }

    public function setNameAgency(string $nameAgency): self
    {
        $this->nameAgency = $nameAgency;

        return $this;
    }

    public function getZipCodeAgency(): ?string
    {
        return $this->zipCodeAgency;
    }

    public function setZipCodeAgency(string $zipCodeAgency): self
    {
        $this->zipCodeAgency = $zipCodeAgency;

        return $this;
    }

    public function getCityAgency(): ?string
    {
        return $this->cityAgency;
    }

    public function setCityAgency(string $cityAgency): self
    {
        $this->cityAgency = $cityAgency;

        return $this;
    }

    public function getNameStreetAgency(): ?string
    {
        return $this->nameStreetAgency;
    }

    public function setNameStreetAgency(string $nameStreetAgency): self
    {
        $this->nameStreetAgency = $nameStreetAgency;

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicle(): Collection
    {
        return $this->vehicle;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicle->contains($vehicle)) {
            $this->vehicle[] = $vehicle;
            $vehicle->setAgency($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->vehicle->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getAgency() === $this) {
                $vehicle->setAgency(null);
            }
        }

        return $this;
    }
}
