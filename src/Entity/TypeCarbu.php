<?php

namespace App\Entity;

use App\Repository\TypeCarbuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeCarbuRepository::class)
 */
class TypeCarbu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $labelCarbu;

    /**
     * @ORM\Column(type="float")
     */
    private $priceLiter;

    /**
     * @ORM\OneToMany(targetEntity=Vehicle::class, mappedBy="typeCarbu")
     */
    private $Vehicle;

    public function __construct()
    {
        $this->Vehicle = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabelCarbu(): ?string
    {
        return $this->labelCarbu;
    }

    public function setLabelCarbu(string $labelCarbu): self
    {
        $this->labelCarbu = $labelCarbu;

        return $this;
    }

    public function getPriceLiter(): ?float
    {
        return $this->priceLiter;
    }

    public function setPriceLiter(float $priceLiter): self
    {
        $this->priceLiter = $priceLiter;

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicle(): Collection
    {
        return $this->Vehicle;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->Vehicle->contains($vehicle)) {
            $this->Vehicle[] = $vehicle;
            $vehicle->setTypeCarbu($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->Vehicle->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getTypeCarbu() === $this) {
                $vehicle->setTypeCarbu(null);
            }
        }

        return $this;
    }
}
