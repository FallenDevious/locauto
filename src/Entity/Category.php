<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $label;

    /**
     * @ORM\Column(type="float")
     */
    private $priceDay;

    /**
     * @ORM\Column(type="float")
     */
    private $priceKm;

    /**
     * @ORM\Column(type="float")
     */
    private $priceWeekEnd;

    /**
     * @ORM\Column(type="float")
     */
    private $priceInsurance;

    /**
     * @ORM\Column(type="float")
     */
    private $guarantee;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nameCreatCat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreatCat;

    /**
     * @ORM\OneToMany(targetEntity=Vehicle::class, mappedBy="category")
     */
    private $vehicle;

    public function __construct()
    {
        $this->vehicle = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getPriceDay(): ?float
    {
        return $this->priceDay;
    }

    public function setPriceDay(float $priceDay): self
    {
        $this->priceDay = $priceDay;

        return $this;
    }

    public function getPriceKm(): ?float
    {
        return $this->priceKm;
    }

    public function setPriceKm(float $priceKm): self
    {
        $this->priceKm = $priceKm;

        return $this;
    }

    public function getPriceWeekEnd(): ?float
    {
        return $this->priceWeekEnd;
    }

    public function setPriceWeekEnd(float $priceWeekEnd): self
    {
        $this->priceWeekEnd = $priceWeekEnd;

        return $this;
    }

    public function getPriceInsurance(): ?float
    {
        return $this->priceInsurance;
    }

    public function setPriceInsurance(float $priceInsurance): self
    {
        $this->priceInsurance = $priceInsurance;

        return $this;
    }

    public function getGuarantee(): ?float
    {
        return $this->guarantee;
    }

    public function setGuarantee(float $guarantee): self
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    public function getNameCreatCat(): ?string
    {
        return $this->nameCreatCat;
    }

    public function setNameCreatCat(string $nameCreatCat): self
    {
        $this->nameCreatCat = $nameCreatCat;

        return $this;
    }

    public function getDateCreatCat(): ?\DateTimeInterface
    {
        return $this->dateCreatCat;
    }

    public function setDateCreatCat(\DateTimeInterface $dateCreatCat): self
    {
        $this->dateCreatCat = $dateCreatCat;

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicle(): Collection
    {
        return $this->vehicle;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicle->contains($vehicle)) {
            $this->vehicle[] = $vehicle;
            $vehicle->setCategory($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->vehicle->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getCategory() === $this) {
                $vehicle->setCategory(null);
            }
        }

        return $this;
    }
}
