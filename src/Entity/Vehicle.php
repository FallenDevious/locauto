<?php

namespace App\Entity;

use App\Repository\VehicleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VehicleRepository::class)
 */
class Vehicle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $model;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $brand;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePurchase;

    /**
     * @ORM\Column(type="float")
     */
    private $mileage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $fuel;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $licensePlate;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nameCreat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreat;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="vehicle")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=TypeCarbu::class, inversedBy="Vehicle")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeCarbu;

    /**
     * @ORM\ManyToOne(targetEntity=Agency::class, inversedBy="vehicle")
     * @ORM\JoinColumn(nullable=false)
     */
    private $agency;

    /**
     * @ORM\OneToMany(targetEntity=Reserve::class, mappedBy="veehicle")
     */
    private $reserves;

    /**
     * @ORM\Column(type="float")
     */
    private $refueling;

    /**
     * @ORM\Column(type="boolean")
     */
    private $rent;
    
    public function __construct()
    {
        $this->reserves = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getDatePurchase(): ?\DateTimeInterface
    {
        return $this->datePurchase;
    }

    public function setDatePurchase(\DateTimeInterface $datePurchase): self
    {
        $this->datePurchase = $datePurchase;

        return $this;
    }

    public function getMileage(): ?float
    {
        return $this->mileage;
    }

    public function setMileage(float $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getFuel(): ?float
    {
        return $this->fuel;
    }

    public function setFuel(?float $fuel): self
    {
        $this->fuel = $fuel;

        return $this;
    }

    public function getLicensePlate(): ?string
    {
        return $this->licensePlate;
    }

    public function setLicensePlate(string $licensePlate): self
    {
        $this->licensePlate = $licensePlate;

        return $this;
    }

    public function getNameCreat(): ?string
    {
        return $this->nameCreat;
    }

    public function setNameCreat(string $nameCreat): self
    {
        $this->nameCreat = $nameCreat;

        return $this;
    }

    public function getDateCreat(): ?\DateTimeInterface
    {
        return $this->dateCreat;
    }

    public function setDateCreat(\DateTimeInterface $dateCreat): self
    {
        $this->dateCreat = $dateCreat;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getTypeCarbu(): ?TypeCarbu
    {
        return $this->typeCarbu;
    }

    public function setTypeCarbu(?TypeCarbu $typeCarbu): self
    {
        $this->typeCarbu = $typeCarbu;

        return $this;
    }

    public function getAgency(): ?Agency
    {
        return $this->agency;
    }

    public function setAgency(?Agency $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    public function getRefueling(): ?float
    {
        return $this->refueling;
    }

    public function setRefueling(float $refueling): self
    {
        $this->refueling = $refueling;

        return $this;
    }

    public function getRent(): ?bool
    {
        return $this->rent;
    }

    public function setRent(bool $rent): self
    {
        $this->rent = $rent;

        return $this;
    }
}
