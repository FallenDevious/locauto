<?php

namespace App\Entity;

use App\Repository\ReserveRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReserveRepository::class)
 */
class Reserve
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $takenInsurance;

    /**
     * @ORM\Column(type="float")
     */
    private $estimatedPrice;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $returnDate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $kmReturn;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $priceReal;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nameCreat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreat;

    /**
     * @ORM\ManyToOne(targetEntity=Vehicle::class, inversedBy="reserves")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vehicle;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getTakenInsurance(): ?bool
    {
        return $this->takenInsurance;
    }

    public function setTakenInsurance(bool $takenInsurance): self
    {
        $this->takenInsurance = $takenInsurance;

        return $this;
    }

    public function getEstimatedPrice(): ?float
    {
        return $this->estimatedPrice;
    }

    public function setEstimatedPrice(float $estimatedPrice): self
    {
        $this->estimatedPrice = $estimatedPrice;

        return $this;
    }

    public function getReturnDate(): ?\DateTimeInterface
    {
        return $this->returnDate;
    }

    public function setReturnDate(?\DateTimeInterface $returnDate): self
    {
        $this->returnDate = $returnDate;

        return $this;
    }

    public function getKmReturn(): ?float
    {
        return $this->kmReturn;
    }

    public function setKmReturn(?float $kmReturn): self
    {
        $this->kmReturn = $kmReturn;

        return $this;
    }

    public function getPriceReal(): ?float
    {
        return $this->priceReal;
    }

    public function setPriceReal(?float $priceReal): self
    {
        $this->priceReal = $priceReal;

        return $this;
    }

    public function getNameCreat(): ?string
    {
        return $this->nameCreat;
    }

    public function setNameCreat(string $nameCreat): self
    {
        $this->nameCreat = $nameCreat;

        return $this;
    }

    public function getDateCreat(): ?\DateTimeInterface
    {
        return $this->dateCreat;
    }

    public function setDateCreat(\DateTimeInterface $dateCreat): self
    {
        $this->dateCreat = $dateCreat;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
