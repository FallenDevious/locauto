<?php

namespace App\Controller;

use Knp\Snappy\Pdf;
use App\Entity\User;
use App\Entity\Reserve;
use App\Entity\Vehicle;
use App\Entity\Category;
use App\Form\ReserveType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LocautoController extends AbstractController
{
    private function reserve(){
    }
    /**
     * @Route("/", name="locauto")
     */
    public function index(Request $request, EntityManagerInterface $manager): Response
    {
        $latitude = 48.686096421090234;
$longitude =6.163222987213132;
if (isset($_POST['envoyer'])) {
    $adresse = $_POST['adresse'];
    $cp = $_POST['cp'];
    $ville = $_POST['ville'];
    $pays = $_POST['pays'];


    #construction du lien demander par l api open-street
    $geoadresse = $adresse." ".$cp." ".$ville." ".$pays;
    $addressUrlEncoded = urlencode($geoadresse);
    $server = "http://maps.open-street.fr/";
    $stringUrl = "api/geocoding/";
    $key = "56ed4756ea68b711128ede1f71ac1a81";
    $fullUrl = $server.$stringUrl."?address=".$addressUrlEncoded."&sensor=false&key=".$key;

    #lecture du fichier json reçu par open-street
    $ContentJson = file_get_contents($fullUrl);

    #decodage du fichier
    $arrayJson = json_decode($ContentJson, true);

    $adresse = $arrayJson['results'][0]['formatted_address'][0];

    $latitude = $arrayJson['results'][0]['geometry']['location']['lat'];
    $longitude = $arrayJson['results'][0]['geometry']['location']['lng'];
    $status=$arrayJson['status'];
}
    $token = "b072182198451b3a65adf84b5063a2507cab7d77fe1660b38973674a62931df6";
    $url = 'https://api.meteo-concept.com/api/forecast/daily?token='.$token.'&latlng='.$latitude.','.$longitude;
    $dataWeather = file_get_contents($url);

    $decodedDataWeather = json_decode($dataWeather);

    $city = $decodedDataWeather->city;
    $forecasts = $decodedDataWeather->forecast;

	foreach ($forecasts as $k => $f) {
		$day = (new \DateTime($f->datetime))->format('w');
		if ($day == 6) {
			$saturday = $k;
			break;
		}
	}
    $today = (new \DateTime($forecasts[0]->datetime))->format('l');
    $tomorrow = (new \DateTime($forecasts[1]->datetime))->format('l');
    $afterTomorrow = (new \DateTime($forecasts[2]->datetime))->format('l');
    $day4 = (new \DateTime($forecasts[3]->datetime))->format('l');
    $day5 = (new \DateTime($forecasts[4]->datetime))->format('l');
    $day6 = (new \DateTime($forecasts[5]->datetime))->format('l');
    $day7 = (new \DateTime($forecasts[6]->datetime))->format('l');
 
        $reserve = new Reserve();
        
        $repo = $this->getDoctrine()->getRepository(Category::class);
        $categories = $repo->findall();
        $repoVehicle = $this->getDoctrine()->getRepository(Vehicle::class);
        
        $formReservation = $this->createForm(ReserveType::class, $reserve);
        
        $formReservation->handleRequest($request);
        
        
        return $this->render('locauto/index.html.twig', [
            'controller_name' => 'LocautoController',
             'formReservation' => $formReservation->createView(),
             'categories' => $categories,
             'today' => $today,
             'tomorrow' => $tomorrow,
             'afterTomorrow' => $afterTomorrow,
             'day4' => $day4,
             'day5' => $day5,
             'day6' => $day6,
             'day7' => $day7,
             'forecasts' => $forecasts,
             'city' => $city
        ]);
    }

    /**
     * @Route("/confirmReserve", name ="confirm_reserve")
     */
    public function confirm(Request $request, EntityManagerInterface $manager, \Swift_Mailer $mailer,\Knp\Snappy\Pdf $snappy){
        if (isset($_POST['return'])) {
            return $this->redirectToRoute('locauto');
        }
        dump($request);
        $reserve = new Reserve();
        $user = $this->getUser();
        

        $repo = $this->getDoctrine()->getRepository(Category::class);
        $categories = $repo->findall();
        
        
        $formReservation = $this->createForm(ReserveType::class, $reserve);
        $formReservation->handleRequest($request);
        
        if ($user) {
            $nameUser = $this->getUser()->getUsername();
            $reserve->setNameCreat($nameUser);
        }
        
        $category = $repo->find($formReservation->get('categorie')->getData());

        $startDate = $formReservation->get('startDate')->getData();
        $startDay = $formReservation->get('startDate')->getData()->format('l');
        $endDate = $formReservation->get('endDate')->getData();
        $endDay = $formReservation->get('endDate')->getData()->format('l');

        if ($startDay == 'Friday' && $endDay == 'Sunday') {
            $remise = 0;
            $Guarantee = $category->getGuarantee();
            $diff = $endDate->diff($startDate);
            $nbDays = $diff->d +1;
            $priceday = $category->getPriceDay();
            $priceDays = $priceday * $nbDays; 
            $priceWeekEnd = $category->getPriceWeekEnd();
            if ($formReservation->get('takenInsurance')->getData() == 1) {
                $PriceInsurance = $category->getPriceInsurance();
            }else {
                $PriceInsurance = 0;
            }
            $estimatedPrice =  $priceWeekEnd;
        }else {
            
            $diff = $endDate->diff($startDate);
            $nbDays = $diff->d +1; //  +1 pour qu'il nous compte au moins un jour de location
            $remise = 0;
            $priceday = $category->getPriceDay();
            $priceDays = $priceday * $nbDays; 
            if ($nbDays >= 3) {
                $priceDays = $priceDays - $priceDays * 10 / 100;
                $remise = $priceDays * 10 / 100;
            }
            
            if ($formReservation->get('takenInsurance')->getData() == 1) {
                $PriceInsurance = $category->getPriceInsurance();
            }else {
                $PriceInsurance = 0;
            }
    
            $Guarantee = $category->getGuarantee();
            $estimatedPrice =  $priceDays + $PriceInsurance;
        }
        
       
        if (isset($_POST['confirm'])) {
                if ($formReservation->isSubmitted() && $formReservation->isValid()) {
            
        
                    $reserve->setEstimatedPrice($estimatedPrice);
                    $reserve->setStartDate($startDate);
                    $reserve->setEndDate($endDate);
                    $reserve->setDateCreat(new \datetime());
                    $reserve->setUser($user);

                    $manager->persist($reserve);
                    $manager->flush();

                    $html = $this->renderView('email/pdfReservation.html.twig',[
                        'reservation' => $reserve,
                        'remise' => $remise
                        ]);
                    $this->snappy = $snappy;
                    $filename = 'reservation.pdf';
                    $pdf = $this->snappy->getOutputFromHtml($html);
                    $message = (new \Swift_Message('reservation'))
                    ->setSubject('inscription locauto')
                    ->setFrom('locauto.contact@gmail.com')
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'email/ReservationEmail.html.twig'),
                        'text/html'
                    );
                    
                    //join PDF
                    $attachement =new \Swift_Attachment($pdf, $filename, 'application/pdf' );
                    $message->attach($attachement);
                    $mailer->send($message);

                    return $this->redirectToRoute('locauto');
                }
            
        }
        return $this->render('locauto/confirmReserve.html.twig',[
            'formReservation' => $formReservation->createView(),
            'estimatedPrice' => $estimatedPrice,
            'Guarantee' => $Guarantee,
            'priceday' => $priceday,
            'pricedays' => $priceDays,
            'nbdays' => $nbDays,
            'priceinsurance' => $PriceInsurance,
            'remise' => $remise,
            'day' => $startDay
        ]);
    }
}
