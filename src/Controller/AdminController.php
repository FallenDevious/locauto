<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Reserve;
use App\Entity\Vehicle;
use App\Entity\Category;
use App\Form\ReserveType;
use App\Form\ConfirmLocType;
use App\Form\ConfirmLoc2Type;
use App\Form\ReserveEditType;
use App\Form\CreatVehicleType;
use App\Repository\VehicleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

    /**
     * @Route("/admin", name="admin_")
     */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
    
    /**
     * Liste des vehicules
     * 
     * @Route("/vehicle", name="vehicle")
     */
    public function vehicleList(VehicleRepository $vehicles): Response
    {
        $repo = $this->getDoctrine()->getRepository(Vehicle::class);
        $vehicles = $repo->findall();
        return $this->render('admin/vehicle.html.twig',[
            'vehicles' => $vehicles
        ]);
    }

    /**
     * creation de vehicule
     * 
     * @Route("/creatvehicle", name="creat_vehicle")
     */
    public function creatVehicle(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();
        $vehicle = new Vehicle();
        $form = $this->createForm(CreatVehicleType::class, $vehicle);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            if ($user) {
                $nameUser = $this->getUser()->getUsername();
                $vehicle->setNameCreat($nameUser);
                $vehicle->setDateCreat(new \Datetime());
            }

            $manager->persist($vehicle);
            $manager->flush();

            return $this->redirectToRoute('admin_vehicle');
        }
        return $this->render('admin/creatVehicle.html.twig', [
            'form' => $form->createView()
        ]);
    }

     /**
     * vue sur les vehicules de plus de deux ans
     * 
     * @Route("/resellvehicle", name="resell_vehicle")
     */
    public function resellVehicle(){
        $resell = $this->getDoctrine()
    ->getRepository(Vehicle::class)
    ->findResell();
        return $this->render('admin/resellVehicle.html.twig',[
            'resell' => $resell
        ]);
    }
     /**
     * planning des reservations pour un vehicules
     * 
     * @Route("/planning/{id}", name="planning")
     */
    public function planning(Vehicle $vehicle){

        $reservations = $this->getDoctrine()
    ->getRepository(Reserve::class)
    ->findReservations($vehicle);


        return $this->render('admin/planning.html.twig',[
            'reservations' => $reservations
        ]);
    }

    /**
     * Liste des clients
     * 
     * @Route("/userList", name="userList")
     */
    public function userlist(VehicleRepository $users): Response
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $users = $repo->findall();
        return $this->render('admin/userlist.html.twig',[
            'users' => $users
        ]);
    }

    /**
     * planning des reservations d'un client
     * 
     * @Route("/userReversation/{id}", name="userReversation")
     */
    public function userReservation(User $user){

        $reservations = $this->getDoctrine()
    ->getRepository(Reserve::class)
    ->findUserReservations($user);


        return $this->render('admin/userReservations.html.twig',[
            'reservations' => $reservations
        ]);
    }

     /**
     * edition des retour de reservation
     * 
     * @Route("/userReversationEdit/{id}", name="userReversationEdit")
     */
    public function userReservationEdit(Reserve $reservation, Request $request,  EntityManagerInterface $manager, \Swift_Mailer $mailer,\Knp\Snappy\Pdf $snappy){

        if (isset($_POST['return'])) {
            return $this->redirectToRoute('admin_userReversation');
        }
        $repo = $this->getDoctrine()->getRepository(Reserve::class);
        $repoVehicles = $this->getDoctrine()->getRepository(Vehicle::class);
        $reservation = $repo->find($reservation);
        $customer = $reservation->getUser();

        $form = $this->createForm(ReserveEditType::class, $reservation);

        $form->handleRequest($request);


        if (!$reservation) {
            throw $this->createNotFoundException(
                'No reservation found for id '
            );
        }
            if ($form->isSubmitted() && $form->isValid()){
                
                #recuperation du kilometrage a reception du vehicule
                $vehicle = $repoVehicles->find($form->get('vehicle')->getData());
                $kmStart = $vehicle->getMileage();
    
                #recuperation de prix au km du vehicule
                $category = $vehicle->getCategory();
                $priceKm = $category->getPriceKm();
    
                #calcul de la difference de km
                if ($form->get('kmReturn')->getData() < $kmStart) {
                    throw $this->createNotFoundException(
                        'le kilometrage retour est en dessous du kilometrage de depart'
                    );
                }else {
                    $diff = $form->get('kmReturn')->getData() - $kmStart;
                }
                $priceKmTotal = $diff * $priceKm;
                #recuperation de prix au jour du vehicule
                $priceDay = $category->getPriceDay();
    
                #calcul des jour suplémentaire
                $endDate = $reservation->getEndDate();
                $returnDate = $form->get('returnDate')->getData();
                $diffDate = $returnDate->diff($endDate);
                $priceDayAdditional = $diffDate->d * $priceDay;
                #calcul du carburant consommé
                $fuelRemaining = $form->get('fuel')->getData();
                $refueling = $vehicle->getRefueling();
                $fuelConsumed = $refueling - $fuelRemaining;
                $priceFuelConsumed = $fuelConsumed * 1.60;
                $startDay= $reservation->getStartDate()->format('l');
                $endDay = $reservation->getEndDate()->format('l');
                # on verifie si on et en tarif week-end
                if ($startDay == 'Friday' && $endDay == 'Sunday') {
                    if ($diff > 1000) {
                        $diff = $diff - 1000;
                        $priceKmTotal = $diff * $priceKm;
                    }else {
                        $priceKmTotal = 0;
                    }
                }
                #ajout du prix des km, du carburant et jour en plus
                $priceReel = $reservation->getEstimatedPrice() + $priceKmTotal + $priceDayAdditional + $priceFuelConsumed;
                $reservation->setReturnDate($returnDate);
                $reservation->setPriceReal($priceReel);
                
                $vehicle->setRent(false);
                $vehicle->setmileage($form->get('kmReturn')->getData());
    
                $manager->persist($reservation);
                $manager->flush();

                $html = $this->renderView('email/pdfFacture.html.twig',[
                    'reservation' => $reservation
                    ]);
                $this->snappy = $snappy;
                $filename = 'reservation.pdf';
                $pdf = $this->snappy->getOutputFromHtml($html);
                $message = (new \Swift_Message('reservation'))
                ->setSubject('Facture locauto')
                ->setFrom('locauto.contact@gmail.com')
                ->setTo($customer->getEmail())
                ->setBody(
                    $this->renderView(
                        'email/factureEmail.html.twig'),
                    'text/html'
                );
                
                //join PDF
                $attachement =new \Swift_Attachment($pdf, $filename, 'application/pdf' );
                $message->attach($attachement);
                $mailer->send($message);
        }
        return $this->render('admin/userReservationEdit.html.twig',[
            'form' => $form->createView(),
            'reservation' => $reservation
        ]);
    }

    /**
     * planning des reservations en cour
     * 
     * @Route("/reservation/progess", name="reservation_progress")
     */
    public function reservationProgress(){

        $repo = $this->getDoctrine()->getRepository(Reserve::class);
        $reservations = $repo->findall();

        return $this->render('admin/reservationProgress.html.twig',[
            'reservations' => $reservations
        ]);
    }

    /**
     * confirmation de la reservation
     * 
     * @Route("/ConfirmLoc/{id}", name="confirmLoc")
     */
    public function confirmLoc(Reserve $reserve, Request $request, EntityManagerInterface $manager){

        $repo = $this->getDoctrine()->getRepository(Reserve::class);
        $reservation = $repo->find($reserve);

        # recuperation de l utilisateur via la reservation
        $user = $reservation->getUser();
            
        $form = $this->createForm(ConfirmLocType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $user->getNameUser($form->get('nameUser')->getData());
            
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('admin_confirmLoc2',[
                'id' => $reserve->getId()
            ]);
        }

    return $this->render('admin/reservationConfirm.html.twig',[
            'form' => $form->createView(),
            'reservation' => $reservation
        ]);
}

    /**
     *update du kilometrage avant la location
     * @Route("/ConfirmLoc2/{id}", name="confirmLoc2")
     */
    public function confirmLoc2(Request $request,Reserve $reserve,  EntityManagerInterface $manager){

        
        $repo = $this->getDoctrine()->getRepository(Reserve::class);
        $reservation = $repo->find($reserve);

        $vehicle = $reservation->getVehicle();

        $form = $this->createForm(ConfirmLoc2Type::class, $vehicle,['category' => $vehicle->getCategory()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $vehicle->setfuel($vehicle->getRefueling());
            $vehicle->setRent(true);
            $manager->persist($vehicle);
            $manager->flush();

            return $this->redirectToRoute('admin_userList',[
                'id' => $reserve->getId()
            ]);
        }

        return $this->render('admin/reservationConfirm2.html.twig',[
            'form' => $form->createView(),
            'reservation' => $reservation
        ]);
    }
    /**
     * @Route("/reservation/{id}", name="reservation_admin")
     */
    public function reservationAdmin(Request $request, User $user, EntityManagerInterface $manager, \Swift_Mailer $mailer,\Knp\Snappy\Pdf $snappy): Response
    {
        dump($request);

        $reserve = new Reserve();
        $repo = $this->getDoctrine()->getRepository(User::class);
        $customer =  $repo->find($user);
        $userAdmin = $this->getUser();

        
        
        $repo = $this->getDoctrine()->getRepository(Category::class);
        $categories = $repo->findall();
        $repoVehicle = $this->getDoctrine()->getRepository(Vehicle::class);
        
        $formReservation = $this->createForm(ReserveType::class, $reserve);
        
        $formReservation->handleRequest($request);

        

        if ($formReservation->isSubmitted() && $formReservation->isValid()){

            $category = $repo->find($formReservation->get('categorie')->getData());

        $startDate = $formReservation->get('startDate')->getData();
        $startDay = $formReservation->get('startDate')->getData()->format('l');
        $endDate = $formReservation->get('endDate')->getData();
        $endDay = $formReservation->get('endDate')->getData()->format('l');
        $priceKm = $category->getPriceKm();

        if ($startDay == 'Friday' && $endDay == 'Sunday') {
            $remise = 0;
            $Guarantee = $category->getGuarantee();
            $diff = $endDate->diff($startDate);
            $nbDays = $diff->d +1;
            $priceday = $category->getPriceDay();
            $priceDays = $priceday * $nbDays; 
            $priceWeekEnd = $category->getPriceWeekEnd();
            if ($formReservation->get('takenInsurance')->getData() == 1) {
                $PriceInsurance = $category->getPriceInsurance();
            }else {
                $PriceInsurance = 0;
            }
            $estimatedPrice =  $priceWeekEnd + $PriceInsurance;
        }else {
            
            $diff = $endDate->diff($startDate);
            $nbDays = $diff->d +1; //  +1 pour qu'il nous compte au moins un jour de location
            $remise = 0;
            $priceday = $category->getPriceDay();
            $priceDays = $priceday * $nbDays; 
            if ($nbDays >= 3) {
                $priceDays = $priceDays - $priceDays * 10 / 100;
                $remise = $priceDays * 10 / 100;
            }
            
            if ($formReservation->get('takenInsurance')->getData() == 1) {
                $PriceInsurance = $category->getPriceInsurance();
            }else {
                $PriceInsurance = 0;
            }
    
            $Guarantee = $category->getGuarantee();
            $estimatedPrice =  $priceDays + $PriceInsurance;
        }

            $reserve->setEstimatedPrice($estimatedPrice);
            if ($user) {
                $nameUser = $this->getUser()->getUsername();
                $reserve->setNameCreat($nameUser);
            }
                    $reserve->setStartDate($startDate);
                    $reserve->setEndDate($endDate);
                    $reserve->setDateCreat(new \datetime());
                    $reserve->setUser($customer);
            $manager->persist($reserve);
            $manager->flush();

            $html = $this->renderView('email/pdfReservation.html.twig',[
                'reservation' => $reserve,
                'remise' => $remise
                ]);
            $this->snappy = $snappy;
            $filename = 'reservation.pdf';
            $pdf = $this->snappy->getOutputFromHtml($html);
            $message = (new \Swift_Message('reservation'))
            ->setSubject('Facture locauto')
            ->setFrom('locauto.contact@gmail.com')
            ->setTo($customer->getEmail())
            ->setBody(
                $this->renderView(
                    'email/ReservationEmail.html.twig'),
                'text/html'
            );
            
            //join PDF
            $attachement =new \Swift_Attachment($pdf, $filename, 'application/pdf' );
            $message->attach($attachement);
            $mailer->send($message);

            return $this->redirectToRoute('admin_userReversation',[
                'id' => $customer->getId()
            ]);
        }
        
        return $this->render('locauto/index.html.twig', [
            'controller_name' => 'LocautoController',
             'formReservation' => $formReservation->createView(),
             'categories' => $categories,
        ]);
    }
}
