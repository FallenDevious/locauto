<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Agency;
use App\Entity\Reserve;
use App\Entity\Vehicle;
use App\Entity\Category;
use App\Entity\TypeCarbu;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CategoryFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        #creation des catégories des vehicules
        $categoryA = new Category();
        $categoryA->setLabel("Economique")
                ->setPriceDay(15)
                ->setPriceKm(0.10)
                ->setPriceWeekEnd(40)
                ->setPriceInsurance(8)
                ->setGuarantee(800)
                ->setNameCreatCat("admin")
                ->setDateCreatCat(new \Datetime());

        $manager->persist($categoryA);

        $categoryB = new Category();
        $categoryB->setLabel("Classe Moyenne")
                ->setPriceDay(20)
                ->setPriceKm(0.15)
                ->setPriceWeekEnd(55)
                ->setPriceInsurance(10)
                ->setGuarantee(900)
                ->setNameCreatCat("admin")
                ->setDateCreatCat(new \Datetime());

        $manager->persist($categoryB);

        $categoryC = new Category();
        $categoryC->setLabel("Luxe")
                ->setPriceDay(30)
                ->setPriceKm(0.25)
                ->setPriceWeekEnd(85)
                ->setPriceInsurance(12)
                ->setGuarantee(1000)
                ->setNameCreatCat("admin")
                ->setDateCreatCat(new \Datetime());
        $manager->persist($categoryC);

        #creation des types de carburant
        $petrol = new TypeCarbu();
        $petrol->setLabelCarbu("essence")
            ->setPriceLiter(1.60);

        $manager->persist($petrol);

        #creation des agences
        $agency = new Agency();
        $agency->setNameAgency("Locauto Nancy")
            ->setZipCodeAgency("54000")
            ->setCityAgency("Nancy")
            ->setNameStreetAgency("235 rue des Grands Platanes");
        
        $manager->persist($agency);

        #Creation des vehicule

        #CATEGORIE A
        $car1 = new Vehicle();
        $car1->setModel("Sandero")
            ->setBrand("Dacia")
            ->setDatePurchase(new \Datetime('2020/03/05'))
            ->setMileage(150)
            ->setFuel(50)
            ->setLicensePlate("AA-123-AA")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryA)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(true);
        
        $manager->persist($car1);

        $car2 = new Vehicle();
        $car2->setModel("Logan")
            ->setBrand("Dacia")
            ->setDatePurchase(new \Datetime('2021/05/15'))
            ->setMileage(50)
            ->setFuel(50)
            ->setLicensePlate("LH-244-KE")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryA)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(true);
        
        
        $manager->persist($car2);

        $car3 = new Vehicle();
        $car3->setModel("C1")
            ->setBrand("Citroen")
            ->setDatePurchase(new \Datetime('2020/01/28'))
            ->setMileage(180)
            ->setFuel(50)
            ->setLicensePlate("IK-654-SD")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryA)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(false);
        
        
        $manager->persist($car3);

        #CATEGORIE B

        $car4 = new Vehicle();
        $car4->setModel("C4")
            ->setBrand("Citroen")
            ->setDatePurchase(new \Datetime('2020/07/16'))
            ->setMileage(156)
            ->setFuel(50)
            ->setLicensePlate("GD-654-BF")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryB)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(false);
        
        
        $manager->persist($car4);

        $car5 = new Vehicle();
        $car5->setModel("A250")
            ->setBrand("Mercedes")
            ->setDatePurchase(new \Datetime('2021/03/11'))
            ->setMileage(180)
            ->setFuel(50)
            ->setLicensePlate("DF-648-LE")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryB)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(false);
        
        
        $manager->persist($car5);

        $car6 = new Vehicle();
        $car6->setModel("508")
            ->setBrand("Peugeot")
            ->setDatePurchase(new \Datetime('2021/01/11'))
            ->setMileage(25)
            ->setFuel(50)
            ->setLicensePlate("XG-945-IS")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryB)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(false);
        
        
        $manager->persist($car6);

        #CATEGORIE C
        $car7 = new Vehicle();
        $car7->setModel("S 65 AMG")
            ->setBrand("Mercedes")
            ->setDatePurchase(new \Datetime('2021/04/01'))
            ->setMileage(0)
            ->setFuel(50)
            ->setLicensePlate("TG-950-KL")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryC)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(false);
        
        
        $manager->persist($car7);

        $car8 = new Vehicle();
        $car8->setModel("A8 S8")
            ->setBrand("Audi")
            ->setDatePurchase(new \Datetime('2020/11/30'))
            ->setMileage(25)
            ->setFuel(50)
            ->setLicensePlate("GH-354-IF")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryC)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(false);
        
        $manager->persist($car8);

        $car9 = new Vehicle();
        $car9->setModel("760i")
            ->setBrand("BMW")
            ->setDatePurchase(new \Datetime('2016/08/24'))
            ->setMileage(45)
            ->setFuel(50)
            ->setLicensePlate("TG-950-KL")
            ->setNameCreat("admin")
            ->setDateCreat(new \Datetime())
            ->setColor("blanche")
            ->setCategory($categoryC)
            ->setTypeCarbu($petrol)
            ->setAgency($agency)
            ->setRefueling(50)
            ->setRent(false);

            
            $manager->persist($car9);

            #fixture admin
            $userAdmin = new User();
            $userAdmin->setNameUser('Vezrlorenes')
                      ->setFirstNameUser('Antoine')
                      ->setEmail('locauto.contact@gmail.com')
                      ->setPassword($this->encoder->encodePassword($userAdmin,'administrateur'))
                      ->setZipCode(' 54000')
                      ->setCity('Nancy')
                      ->setNumberLicence('1235487951587')
                      ->setDateLicenseObtained(new \Datetime('2016/08/24'))
                      ->setExpiryDateLicense(new \Datetime('2025/08/24'))
                      ->setPhone('03 83 55 22 22')
                      ->setActivateToken(null)
                      ->setAddress('235 rue des Grands Platanes')
                      ->setUsername('admin')
                      ->setRoles(array('ROLE_ADMIN'));

            $manager->persist($userAdmin);

            #fixture client
            $user1 = new User();
            $user1->setNameUser('loic')
                      ->setFirstNameUser('makel')
                      ->setEmail('loic.makel@gmail.com')
                      ->setPassword($this->encoder->encodePassword($user1,'testtest'))
                      ->setZipCode(' 54340')
                      ->setCity('pompey')
                      ->setNumberLicence('021485645245')
                      ->setDateLicenseObtained(new \Datetime('2018/09/13'))
                      ->setExpiryDateLicense(new \Datetime('2025/09/24'))
                      ->setPhone('0642576458')
                      ->setActivateToken(null)
                      ->setAddress('10 des vannes')
                      ->setUsername('loic');

            $manager->persist($user1);
            
            $user2 = new User();
            $user2->setNameUser('philipe')
                      ->setFirstNameUser('hush')
                      ->setEmail('philipe.hush@gmail.com')
                      ->setPassword($this->encoder->encodePassword($user2,'testtest'))
                      ->setZipCode(' 54000')
                      ->setCity('Nancy')
                      ->setNumberLicence('032154687515')
                      ->setDateLicenseObtained(new \Datetime('2020/09/13'))
                      ->setExpiryDateLicense(new \Datetime('2030/09/24'))
                      ->setPhone('0648135487')
                      ->setActivateToken(null)
                      ->setAddress('3 rue de la republique')
                      ->setUsername('philipe');

            $manager->persist($user2);

            $user3 = new User();
            $user3->setNameUser('foo')
            ->setFirstNameUser('barr')
            ->setEmail('foo.barr@gmail.com')
            ->setPassword($this->encoder->encodePassword($user3,'testtest'))
            ->setZipCode('54350')
            ->setCity('frouard')
            ->setNumberLicence('021654987542')
            ->setDateLicenseObtained(new \Datetime('2018/09/13'))
            ->setExpiryDateLicense(new \Datetime('2025/09/24'))
            ->setPhone('0754695812')
            ->setActivateToken(null)
            ->setAddress('235 rue de metz')
            ->setUsername('foo');
            
            $manager->persist($user3);

            $user4 = new User();
            $user4->setNameUser('marcel')
            ->setFirstNameUser('maurin')
            ->setEmail('marcel.maurin@gmail.com')
            ->setPassword($this->encoder->encodePassword($user4,'testtest'))
            ->setZipCode('21354')
            ->setCity('monopoly')
            ->setNumberLicence('215468795461')
            ->setDateLicenseObtained(new \Datetime('2011/09/13'))
            ->setExpiryDateLicense(new \Datetime('2019/09/24'))
            ->setPhone('07654213548')
            ->setActivateToken(null)
            ->setAddress('235 rue de la paix')
            ->setUsername('marcel');
            
            $manager->persist($user4);

            $user5 = new User();
            $user5->setNameUser('grégoire')
            ->setFirstNameUser('vollay')
            ->setEmail('grégoire.vollay@gmail.com')
            ->setPassword($this->encoder->encodePassword($user5,'testtest'))
            ->setZipCode('54362')
            ->setCity('custine')
            ->setNumberLicence('245987653245')
            ->setDateLicenseObtained(new \Datetime('2011/09/13'))
            ->setExpiryDateLicense(new \Datetime('2019/09/24'))
            ->setPhone('0654897542')
            ->setActivateToken(null)
            ->setAddress('235 rue de la paille')
            ->setUsername('grégoire');
            
            $manager->persist($user5);

            $user6 = new User();
            $user6->setNameUser('sylvie')
            ->setFirstNameUser('bourdin')
            ->setEmail('sylvie.bourdin@gmail.com')
            ->setPassword($this->encoder->encodePassword($user6,'testtest'))
            ->setZipCode('54000')
            ->setCity('Nancy')
            ->setNumberLicence('546875431212')
            ->setDateLicenseObtained(new \Datetime('2011/09/13'))
            ->setExpiryDateLicense(new \Datetime('2019/09/24'))
            ->setPhone('0732549875')
            ->setActivateToken(null)
            ->setAddress('235 place stan')
            ->setUsername('sylvie');
            
            $manager->persist($user6);

            #fixture reservation
            $reservationWeekEnd = new Reserve();
            $reservationWeekEnd->setStartDate(new \Datetime('2021/06/04'))
                        ->setEndDate(new \Datetime('2021/06/06'))
                        ->setTakenInsurance(true)
                        ->setEstimatedPrice(48)
                        ->setNameCreat($user1->getUserName())
                        ->setDateCreat(new \Datetime('2021/06/03'))
                        ->setVehicle($car1)
                        ->setUser($user1)
                        ->setReturnDate(new \Datetime('2021/06/06'))
                        ->setKmReturn(180)
                        ->setPriceReal(48);

            $manager->persist($reservationWeekEnd);

            $reservationWeek = new Reserve();
            $reservationWeek->setStartDate(new \Datetime('2021/05/31'))
                        ->setEndDate(new \Datetime('2021/06/02'))
                        ->setTakenInsurance(true)
                        ->setEstimatedPrice(40.5)
                        ->setNameCreat($user1->getUserName())
                        ->setDateCreat(new \Datetime('2021/05/27'))
                        ->setVehicle($car2)
                        ->setUser($user2)
                        ->setReturnDate(new \Datetime('2021/06/06'))
                        ->setKmReturn(70)
                        ->setPriceReal(44.1);

            $manager->persist($reservationWeek);

            $manager->flush();
    }
}
