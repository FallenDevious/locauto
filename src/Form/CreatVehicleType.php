<?php

namespace App\Form;

use App\Entity\Agency;
use App\Entity\Vehicle;
use App\Entity\Category;
use App\Entity\TypeCarbu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreatVehicleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('model')
        ->add('brand')
        ->add('datePurchase')
        ->add('mileage')
        ->add('fuel')
        ->add('licensePlate')
        ->add('color')

        ->add('category', EntityType::class,[
            'class' => Category::class,
            'choice_label' => 'label'
        ])
        
        ->add('typeCarbu', EntityType::class,[
            'class' => TypeCarbu::class,
            'choice_label' => 'labelCarbu'
        ])
        
        ->add('agency', EntityType::class,[
            'class' => Agency::class,
            'choice_label' => 'nameAgency'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vehicle::class,
        ]);
    }
}
