<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Reserve;
use App\Entity\Vehicle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ReserveEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateType::class, [
                // renders it as a single text box
                'widget' => 'single_text'
            ])
            ->add('endDate',DateType::class,  [
                // renders it as a single text box
                'widget' => 'single_text'
            ])
            ->add('takenInsurance')
            ->add('estimatedPrice')
            

            ->add('returnDate',DateType::class,  [
                // renders it as a single text box
                'widget' => 'single_text'
                ])
            ->add('kmReturn')

            ->add('vehicle', EntityType::class,[
                'class' => Vehicle::class,
                'choice_label' => 'model'
                    ])
            ->add('fuel',NumberType :: Class,[
                'mapped' => false
                ])
            ->add('user', EntityType::class,[
                'class' => User::class,
                'choice_label' => 'username'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reserve::class,
        ]);
    }
}
