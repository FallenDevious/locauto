<?php

namespace App\Form;

use App\Entity\Vehicle;
use App\Repository\VehicleRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfirmLoc2Type extends AbstractType
{ private $vehicleRepository;
    public function __construct(VehicleRepository $vehicleRepository)
    {
        $this->vehicleRepository = $vehicleRepository;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $category = $options['category'];
        $builder
            ->add('vehicle', EntityType::class,[
                'class' => Vehicle::class,
                'choices'         => $category ? $this->vehicleRepository->findVehicleNotRent($category) : [],
                'choice_label' => 'model',
                'mapped' => false
            ])
            ->add('mileage')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vehicle::class,
            $resolver->setRequired(['category'])

        ]);
    }
}
