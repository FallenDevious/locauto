<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ConfirmLocType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nameUser')
        ->add('firstNameUser')
        ->add('email')
        ->add('address')
        ->add('zipCode')
        ->add('city')
        ->add('phone')
        ->add('numberLicence')
        ->add('dateLicenseObtained',DateType::class,  [
            'widget' => 'single_text'
        ])
        ->add('expiryDateLicense',DateType::class,  [
            // renders it as a single text box
            'widget' => 'single_text'
        ])
    ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
