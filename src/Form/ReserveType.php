<?php

namespace App\Form;

use App\Entity\Reserve;
use App\Entity\Vehicle;
use App\Entity\Category;
use Doctrine\ORM\EntityRepository;
use App\Repository\VehicleRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\WeekType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class ReserveType extends AbstractType
{
  private $vehicleRepository;
  public function __construct(VehicleRepository $vehicleRepository)
  {
      $this->vehicleRepository = $vehicleRepository;
  }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate',DateType::class,  [
                // renders it as a single text box
                'widget' => 'single_text',
                'data' => new \DateTime(),
                'mapped' => false
            ])
            ->add('endDate',DateType::class,  [
                // renders it as a single text box
                'widget' => 'single_text',
                'data' => new \DateTime(),
                'mapped' => false
            ])
            ->add('takenInsurance')

            ->add('categorie', EntityType::class,[
                'class' => Category::class,
                'placeholder' => 'Sélectionnez votre categorie',
                'choice_label' => 'label',
                'mapped' => false
            ])
            ;
            # le champs véhicules ne s affiche pas dans le formulaire de reservation
            # Mais ce reste du code permet de selection le premier véhicule de la catégorie 
            #sans le mettre indisponible
            #un moyen de contourné la contrainte du vehicule qui ne peut etre null
            $builder->get('categorie')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event){
                    $form = $event->getForm();
                    $this->addVehicleField($form->getParent(), $form->getData());
                }
            );
            $builder->addEventListener(
                FormEvents::POST_SET_DATA,
                function (FormEvent $event) {
                  $data = $event->getData();
                  
                  $vehicle = $data->getVehicle();
                  $form = $event->getForm();
                  if ($vehicle) {
                    // On récupère les vehicules et la catégorie
                    $category = $vehicle->getCategory();
                    $this->addVehicleField($form, $category);
                    // On set les données
                    $form->get('categorie')->setData($category);
                  }
                  else {
                    // On crée le champ en les laissant vide (champs utilisé pour le JavaScript)
                    $this->addVehicleField($form, null);
                  }
                }
              );
    }
    private function addVehicleField(FormInterface $form, ?Category $category)
{

  $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
    'vehicle',
    EntityType::class,
    null,
    [
      'class'           => Vehicle::class,
      'choice_label' => 'model',
      'choices'         => $category ? $this->vehicleRepository->findVehicleNotRent($category) : [],
      'auto_initialize' => false,
      'required' => true,
    ]
  );
  $form->add($builder->getForm());
}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reserve::class,
        ]);
    }
}
